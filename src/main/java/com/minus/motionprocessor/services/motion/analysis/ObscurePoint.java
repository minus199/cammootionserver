package com.minus.motionprocessor.services.motion.analysis;

/**
 * Created by minus on 10/20/16.
 */
class ObscurePoint {
	private final int x;
	private final int y;
	private final int r;
	private final int g;
	private final int b;

	private final int delta;
	private final int col;
	private final int pixel;

	ObscurePoint(int x, int y, int r, int g, int b) {
		this.x = x;
		this.y = y;

		this.r = r;
		this.g = g;
		this.b = b;

		this.delta = r + g + b;
		this.pixel = ((r & 0x0ff) << 16) | ((g & 0x0ff) << 8) | (b & 0x0ff);
		this.col = (r << 16) | (g << 8) | b;
	}

	public int getDelta() {
		return delta;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getPixel() {
		return pixel;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (! (o instanceof ObscurePoint)) return false;

		ObscurePoint point = (ObscurePoint) o;

		if (getX() != point.getX()) return false;
		return getY() == point.getY();
	}

	@Override
	public int hashCode() {
		int result = getX();
		result = 31 * result + getY();
		return result;
	}
}