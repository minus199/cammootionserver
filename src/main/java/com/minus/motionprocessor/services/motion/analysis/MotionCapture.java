package com.minus.motionprocessor.services.motion.analysis;

/**
 * Created by minus on 10/16/16.
 */

import com.minus.motionprocessor.services.motion.Camera;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.opencv.core.Core;

public class MotionCapture extends Application {


	@Override
	public void start(Stage primaryStage) {
		try {
			MainController mainController = new MainController();

			Scene scene = new Scene(mainController, 800, 600);
			primaryStage.setTitle("JavaFX meets OpenCV");
			primaryStage.setScene(scene);
			primaryStage.show();

			mainController.initController();

			primaryStage.setOnCloseRequest((we -> {
				mainController.setClosed();
				System.out.println("Closed");
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}