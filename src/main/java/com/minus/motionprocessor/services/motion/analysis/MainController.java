package com.minus.motionprocessor.services.motion.analysis;

/**
 * Created by minus on 10/16/16.
 */

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import org.opencv.videoio.VideoCapture;

/**
 * The controller for our application, where the application logic is
 * implemented. It handles the button for starting/stopping the camera and the
 * acquired video stream.
 *
 * @author <a href="mailto:luigi.derussis@polito.it">Luigi De Russis</a>
 * @author Maximilian Zuleger - some important fixes <max-z.de>
 * @version 1.6 (2016-8-16)
 * @since 1.0 (2013-10-20)
 */
public class MainController extends BorderPane {

//	private static int videodevice = 0;

	private Button button;
	private ImageView currentFrame;
	private ImageView deltaFrame;
	private Pane imageViewPane;
	private Text fps;
	private Text currentFps;



	public void initController() {
		/*button = new Button("Start cam");
		button.setOnAction(this::startCamera);
		fps = new Text("FPS");
		currentFps = new Text("");
		setBottom(button);
		setTop(new HBox(fps, currentFps));
		currentFrame = new ImageView();
//		deltaFrame = new ImageView();
//		setLeft(new Pane(deltaFrame));
		imageViewPane = new Pane(currentFrame);
		setCenter(imageViewPane);*/

	}

	/**
	 * The action triggered by pushing the button on the GUI
	 *
	 * @param event the push button event
	 */
	@FXML
	protected void startCamera(ActionEvent event) {

	}

	public void setClosed() {
//		cameraActive = false;
//		capture.release();
	}
}