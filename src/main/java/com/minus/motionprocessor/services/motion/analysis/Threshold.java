package com.minus.motionprocessor.services.motion.analysis;

import com.google.common.util.concurrent.AtomicDouble;
import com.minus.motionprocessor.entities.motion.EventStatistics;
import org.opencv.core.Mat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

/**
 * Created by minus on 10/21/16.
 */
public class Threshold {
	private static final Logger logger = LoggerFactory.getLogger(Threshold.class);

	private final ConcurrentLinkedDeque<Double> deltaValues = new ConcurrentLinkedDeque<>();
	private final AtomicDouble threshold = new AtomicDouble(0.185);

	private final ReentrantLock adjustingLock = new ReentrantLock();
	private final RoundingMode roundingMode;
	private int scale = 2;

	Threshold(int scale, RoundingMode roundingMode) {
		this.scale = scale;
		this.roundingMode = roundingMode;
	}

	/**
	 * @param benchMarkSupplier    the benchmark against current frames will be diffed.
	 * @param currentFrameSupplier the continues frame supplier.
	 *                             Will be grabbed for the duration of <code>learningPeriod</code> at <code>intervals</code> intervals
	 * @param learningPeriod       for <code>currentFrameSupplier</code>
	 * @param intervals            for <code>currentFrameSupplier</code>
	 * @return EventStatistics
	 */
	EventStatistics compute(Supplier<BufferedImage> benchMarkSupplier, Supplier<Mat> currentFrameSupplier,
	                        long learningPeriod, long intervals) {

		long start = System.currentTimeMillis();

		AtomicDouble sum = new AtomicDouble();
		AtomicInteger counter = new AtomicInteger();

		while (System.currentTimeMillis() - start < learningPeriod) {
			ComputedDiff computedDiff = ImgDiff
					.diff(benchMarkSupplier.get(), currentFrameSupplier.get())
					.orElse(Engine.zeroDiff);

			if (computedDiff.getTotal() == 0.0) {
				logger.debug("0 value - waiting");
				try { Thread.sleep(1000L); } catch (InterruptedException e1) {}
				start = System.currentTimeMillis();
			}

			sum.getAndAdd(computedDiff.getTotal());

			long l = 100 * (System.currentTimeMillis() - start) / learningPeriod;
			logger.debug("Progress: {}%", l);

			counter.incrementAndGet();

			try { Thread.sleep(intervals); } // intervals between sampling
			catch (InterruptedException e1) {}
		}

		setValue(sum.get() / counter.get());
		logger.debug("After compute threshold is {}", threshold.get());
//		return statsFactory();
		return null;
	}

//	EventStatistics computeConcurrent(Supplier<BufferedImage> benchmarkFrameSupplier, Supplier<Mat> currentFrameSupplier,
//	                                  long learningPeriod, long intervals) {
//
//		long start = System.currentTimeMillis();
//		ArrayList<Double> values = new ArrayList<>();
//		BufferedImage benchmarkFrame = benchmarkFrameSupplier.get();
//
//		ThreadPoolExecutor consumer = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
//
//		ScheduledExecutorService dispatcher = Executors.newSingleThreadScheduledExecutor();
//		/*ScheduledFuture<?> scheduledFuture = dispatcher.scheduleAtFixedRate(() -> consumer.submit(
//				() -> diffFrame(values, benchmarkFrame, currentFrameSupplier.get(), start, learningPeriod)),
//		                                                                    0, intervals, TimeUnit.MILLISECONDS);*/
//
//		new Thread(() -> {
//			long duration = 0;
//			while (duration < learningPeriod) {
//				duration = System.currentTimeMillis() - start;
//			}
//
//			logger.debug("shutting down with {} in queue", consumer.getQueue().size());
//			consumer.shutdown();
//			dispatcher.shutdown();
//		}).start();
//
//		try {
//			consumer.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
//			logger.debug("final shutdown");
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//
////		setValue(values);
//		logger.debug("After compute threshold is {}", threshold.get());
//		return statsFactory();
//	}

	public double value() {
		return new BigDecimal(threshold.doubleValue()).setScale(scale, roundingMode).doubleValue();
	}

	void update(ComputedDiff computedDiff) {
		deltaValues.add(computedDiff.getTotal());
	}

	/*public final EventStatistics statsFactory() {
		return new EventStatistics(this, () -> deltaValues.stream().mapToDouble(v -> v));
	}

	public final EventStatistics statsFactory(Supplier<DoubleStream> deltaValues) {
		return new EventStatistics(this, deltaValues);
	}*/

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Threshold{");
//		sb.append("adjustExecutor=").append(adjustExecutor);
//		sb.append(", adjustingLock=").append(adjustingLock);
		sb.append(", deltaValues=").append(deltaValues);
		sb.append(", lue=").append(value());
		sb.append(", roundingMode=").append(roundingMode);
		sb.append(", scale=").append(scale);
		sb.append(", threshold=").append(threshold);
//		sb.append(", valueSetLock=").append(valueSetLock);
		sb.append('}');
		return sb.toString();
	}

	private void setValue(double val) {
		adjustingLock.tryLock();
		deltaValues.add(val * 1.15);
		adjustingLock.unlock();
	}
}
