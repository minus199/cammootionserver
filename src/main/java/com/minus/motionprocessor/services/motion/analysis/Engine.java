package com.minus.motionprocessor.services.motion.analysis;

import com.minus.motionprocessor.entities.motion.MotionDetectedEvent;
import com.minus.motionprocessor.utils.ImgUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

import static com.minus.motionprocessor.utils.ImgUtils.imgPath;

/**
 * Created by minus on 10/16/16.
 */
public class Engine implements AutoCloseable {
	final static ComputedDiff zeroDiff = new ComputedDiff();


	private static final Logger logger = LoggerFactory.getLogger(Engine.class);
	private static final long calibrateBenchmarkInterval = 5; // MINUTES
	public final MatOfInt params = new MatOfInt(Imgcodecs.CV_IMWRITE_PNG_COMPRESSION, 9);
	private final VideoCapture capture;
	private final ScheduledExecutorService frameGrabberWorker;
	private final ScheduledExecutorService benchmarkUpdater;
	private final ScheduledExecutorService normalizer;
	private final ExecutorService analyzer;
	private final List<ExecutorService> executors;
	private final Threshold threshold;
	private final ReentrantLock currentMatLock = new ReentrantLock();
	private boolean ignited = false;
	private Mat currentMat;
	private MotionDetectedEvent motionDetectedEvent;
	private Mat benchmarkMat;
//	private BufferedImage benchmarkBuffered;

	public Engine(VideoCapture capture) {

		this.capture = capture;
		threshold = new Threshold(2, RoundingMode.UP);
		motionDetectedEvent = new MotionDetectedEvent(/*threshold*/);

		frameGrabberWorker = Executors.newSingleThreadScheduledExecutor(getThreadFactory("frameGrabberWorker"));
		normalizer = Executors.newSingleThreadScheduledExecutor(getThreadFactory("normalizer"));
		analyzer = Executors.newFixedThreadPool(3, getThreadFactory("analyzer"));
		benchmarkUpdater = Executors.newSingleThreadScheduledExecutor(getThreadFactory("benchmarkUpdater"));

		executors = Arrays.asList(frameGrabberWorker, normalizer, analyzer, benchmarkUpdater);
	}



	private ThreadFactory getThreadFactory(final String threadName) {
		return r -> {
			Thread thread = new Thread(r);
			thread.setName(threadName);
			thread.setUncaughtExceptionHandler((t, e) -> logger.debug("General error [{}] madafucka on thread {}", e.getMessage(), t.getName()));
			return thread;
		};
	}



	public void ignition() {
		updateCurrentMat();

		while (! Thread.currentThread().isInterrupted()) {
			Mat imageOne = normalizeFrame(getCurrentMat());
			BufferedImage bufferedOne = ImgUtils.toBufferedImageArrayCopy(imageOne);

			updateCurrentMat();
			Mat imageTwo = normalizeFrame(getCurrentMat());

			BufferedImage bufferedTwo = ImgUtils.toBufferedImageArrayCopy(imageTwo);
			double diff = ImgDiff.newDiff(bufferedOne, bufferedTwo) * 1000;
			imageOne.release();
			imageTwo.release();

			logger.debug("{} -- {}", diff, Math.floor(diff));

//			logger.debug("{}", bigDecimal);
			try {
				Thread.sleep(300L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
//		return new BigDecimal(delta / n / 255.0).intValue();

		double sum = 0;
	/*	IntStream.range(0, bufferedOne.getWidth() - 1).map(
				x -> IntStream.range(0, bufferedOne.getHeight() - 1).map(
						y -> {
							int rgb1 = bufferedOne.getRGB(x, y);
							int r1 = (rgb1 >> 16) & 0xff;
							int g1 = (rgb1 >> 8) & 0xff;
							int b1 = (rgb1) & 0xff;

							int rgb2 = bufferedTwo.getRGB(x, y);
							int r2 = (rgb2 >> 16) & 0xff;
							int g2 = (rgb2 >> 8) & 0xff;
							int b2 = (rgb2) & 0xff;

							double n = x * y * 3;
							int delta = Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2);

							return new BigDecimal(delta / n / 255.0).intValue();
						}).sum()).average();*/

	/*	double rD = 0;
		double gD = 0;
		double bD = 0;

		for (int width = 0; width < imageOne.cols(); width++) {
			for (int height = 0; height < imageOne.rows(); height++) {

				double[] doubles = imageOne.get(height, width);
				rD += doubles[0];
				gD += doubles[1];
				bD += doubles[2];
			}
		}
		rD = rD / (imageOne.cols() * imageOne.rows());
		gD = gD / (imageOne.cols() * imageOne.rows());
		bD = bD / (imageOne.cols() * imageOne.rows());
		long l = System.currentTimeMillis();
		Core.absdiff(imageTwo, imageOne, dest);
		long elapsed = System.currentTimeMillis() - l;
		System.out.println(elapsed);*/

		System.out.println();

		updateCurrentMat();
		resetBenchmark(30000, 70);

		frameGrabberWorker.scheduleAtFixedRate(this::updateCurrentMat, 1000, 50, TimeUnit.MILLISECONDS);
		normalizer.scheduleAtFixedRate(this::processFrame, 5000, 100, TimeUnit.MILLISECONDS);
		benchmarkUpdater.scheduleAtFixedRate(() -> this.resetBenchmark(30000, 100), calibrateBenchmarkInterval, calibrateBenchmarkInterval, TimeUnit.MINUTES);

		ignited = true;
	}

	private void processFrame() {
		Mat currentMat = getCurrentMat();
		normalizeFrame(currentMat); // Processing before writing the frame to HD

		analyzer.submit(() -> {
			Mat diff = new Mat();
			Core.absdiff(benchmarkMat, currentMat, diff);
			System.out.println();

//			ComputedDiff cDiff = ImgDiff.diff(benchmarkBuffered, currentMat).orElse(zeroDiff);
//			threshold.update(cDiff);
//			if (cDiff.getTotal() > threshold.value()) {
//				logger.debug("Smile :) {}/{}", threshold.value(), cDiff.getTotal());

//				new ImageWriterTask(getCurrentMat(), motionDetectedEvent, params, cDiff).run();
//				new ImageWriterTask(currentMat, motionDetectedEvent, params, cDiff).run();
//			}

			currentMat.release();
		});
	}

	private void resetBenchmark(long learningPeriod, long intervals) {
		//check for diff between new image and last image. continue reseting until diff stabled.
		Path benchmarkPath = imgPath("benchmark");

		updateCurrentMat();
		Mat tempBenchmarkMat = getCurrentMat();
		normalizeFrame(tempBenchmarkMat);
		logger.debug("New benchmark is ready, will be saved to {}", benchmarkPath.getName(benchmarkPath.getNameCount() - 1));
		new Thread(() -> {
			try {
				BufferedImage bufferedImage = ImgUtils.toBufferedImageArrayCopy(tempBenchmarkMat);
				ImageIO.write(bufferedImage, "png", benchmarkPath.toFile());
			} catch (IOException e) {
				logger.trace("Unable to write benchmark");
			}
		});

		/*threshold.compute(() -> bufferedImage,
		                  () -> {
			                  if (! ignited) updateCurrentMat();
			                  logger.debug("Ongoing event, elapsed: {}", motionDetectedEvent.getElapsed());
			                  return normalizeFrame(getCurrentMat());
		                  },
		                  learningPeriod, intervals);

		// After all computation is done and new benchmark is ready, replace it
		benchmarkBuffered = bufferedImage;*/
	}

	private Mat normalizeFrame(Mat srcMat) {
		Photo.fastNlMeansDenoisingColored(srcMat, srcMat);
		Imgproc.cvtColor(srcMat, srcMat, Imgproc.COLOR_BGR2GRAY);
		return srcMat;
	}

	@Override
	public void close() {
		logger.info("Shutting down...");
		executors.forEach(ExecutorService::shutdown);
		capture.release();
		if (motionDetectedEvent != null) motionDetectedEvent.finalizeEvent();

		logger.info("Goodbye.");
	}

	/**
	 * Grab latest frame into current mat
	 *
	 * @return
	 */
	private void updateCurrentMat() {
		if (! this.capture.isOpened()) return;

		if (currentMat == null) {
			currentMat = new Mat();
		}

		this.capture.read(currentMat);
	}

	private Mat getCurrentMat() {
		if (currentMat == null) {
			currentMat = new Mat();
		}

		return currentMat.clone();
	}
}
