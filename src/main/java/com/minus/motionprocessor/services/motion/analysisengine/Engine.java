package com.minus.motionprocessor.services.motion.analysisengine;

import com.google.common.util.concurrent.AtomicDouble;
import com.minus.motionprocessor.entities.motion.MotionDetectedEvent;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.opencv.imgproc.Imgproc.*;

/**
 * Created by minus on 10/29/16.
 */
public class Engine implements AutoCloseable {
	private static final Logger logger = LoggerFactory.getLogger(Engine.class);

	private static final Mat CONTOUR_KERNEL = getStructuringElement(MORPH_DILATE, new Size(3, 3), new Point(1, 1));
	private static final Mat HIERARCHY = new Mat();
	private static final Point CONTOUR_POINT = new Point(- 1, - 1);
	private static final Point rectPoint1 = new Point();
	private static final Point rectPoint2 = new Point();
	private static final Scalar rectColor = new Scalar(0, 255, 0);
	private final VideoCapture capture;
	private final Mat mat;
	private final Mat workImg;
	private final Mat gray;
	private final Mat diffImg;
	private final Mat scaleImg;
	private final Size kSize = new Size(8, 8);
	private final AtomicInteger motionFrames = new AtomicInteger();
	private final AtomicInteger totalFrames = new AtomicInteger();
	private final AtomicDouble motionPercent = new AtomicDouble();
	private final MotionDetectedEvent motionDetectedEvent;
	private final VideoWriter videoWriter;
	private Mat movingAvgImg;

	public Engine(VideoCapture capture) {
		this.capture = capture;
		mat = new Mat();
		workImg = new Mat();
		movingAvgImg = null;
		gray = new Mat();
		diffImg = new Mat();
		scaleImg = new Mat();
		videoWriter = new VideoWriter();

		motionDetectedEvent = new MotionDetectedEvent(() -> Optional.of(resetCycle()));
	}

	/**
	 * Blocks till interrupted
	 */
	public void ignition() {
		waitTillOpen();

		double totalPixels = getFrameSize().area();

		while (! Thread.currentThread().isInterrupted() && capture.read(mat)) {
			blur(mat, workImg, kSize);
			if (movingAvgImg == null) {
				movingAvgImg = new Mat();
				workImg.convertTo(movingAvgImg, CvType.CV_32F);
			}

			normalizeFrame();
			motionPercent.set(100.0 * Core.countNonZero(gray) / totalPixels);
			adjust();

			totalFrames.incrementAndGet();

			if (motionPercent.doubleValue() > 0.75) registerMotion();
		}
	}

	private void waitTillOpen() {
		while (! capture.isOpened()) {
			logger.debug("Still not opened, waiting");
			try { Thread.sleep(1000L); } catch (InterruptedException e) {}
		}
	}

	private void registerMotion() {
		motionFrames.incrementAndGet();
		double v = motionFrames.doubleValue() / capture.get(Videoio.CAP_PROP_FPS);
		logger.debug("BOOM -- Motion: {}, Current Video Time: {}", motionPercent, v);

		final List<Rect> movementLocations = contours(gray);
		for (final Rect rect : movementLocations) {
			rectPoint1.x = rect.x;
			rectPoint1.y = rect.y;
			rectPoint2.x = rect.x + rect.width;
			rectPoint2.y = rect.y + rect.height;
			rectangle(mat, rectPoint1, rectPoint2, rectColor, 2); // Draw rectangle around fond object
		}

		motionDetectedEvent.addFrame(motionPercent.get());

		videoWriter.write(mat);
	}

	private void adjust() {
		if (motionPercent.doubleValue() > 25.0) { //camera probably adjusting
			workImg.convertTo(movingAvgImg, CvType.CV_32F);
		}
	}

	private void normalizeFrame() {
		accumulateWeighted(workImg, movingAvgImg, .03);
		Core.convertScaleAbs(movingAvgImg, scaleImg);
		Core.absdiff(workImg, scaleImg, diffImg);
		cvtColor(diffImg, gray, COLOR_BGR2GRAY);
		threshold(gray, gray, 25, 255, THRESH_BINARY);
	}

	private List<Rect> contours(final Mat source) {
		Imgproc.dilate(source, source, CONTOUR_KERNEL, CONTOUR_POINT, 15);
		Imgproc.erode(source, source, CONTOUR_KERNEL, CONTOUR_POINT, 10);
		final List<MatOfPoint> contoursList = new ArrayList<MatOfPoint>();
		Imgproc.findContours(source, contoursList, HIERARCHY, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
		final List<Rect> rectList = new ArrayList<Rect>();
		// Convert MatOfPoint to Rectangles
		for (final MatOfPoint mop : contoursList) {
			rectList.add(Imgproc.boundingRect(mop));
			// Release native memory
			mop.release();
		}
		return rectList;
	}

	@Override
	public void close() throws Exception {
		throw new UnsupportedOperationException("Close engine 2");
	}

	private Boolean resetCycle() {
		File currentFilePath = motionDetectedEvent.currentFilePath();
		logger.debug("Opening file {} for videoWriter", currentFilePath.getName());
		videoWriter.open(currentFilePath.toString(), new FourCC("X264").toInt(), capture.get(Videoio.CAP_PROP_FPS), getFrameSize(), true);

		motionFrames.set(0);
		totalFrames.set(0);

		return true;
	}

	private Size getFrameSize() {
		return new Size((int) capture.get(Videoio.CAP_PROP_FRAME_WIDTH), (int) capture.get(Videoio.CAP_PROP_FRAME_HEIGHT));
	}
}
