package com.minus.motionprocessor.services.motion.analysis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by minus on 10/20/16.
 */
public class ComputedDiff {
	private static final Logger logger = LoggerFactory.getLogger(Engine.class);

	private final Integer diff;
	private final BigDecimal total;
	private final BigDecimal normalized;
	private final AtomicInteger scale = new AtomicInteger(2);
	private RoundingMode roundingMode = RoundingMode.UP;

	ComputedDiff() {
		this.diff = 0;
		this.total = new BigDecimal(0);
		this.normalized = new BigDecimal(0);
	}

	ComputedDiff(final int width, final int height, List<ObscurePoint> points) {
		double n = width * height * 3;
		diff = points.stream().collect(Collectors.summingInt(ObscurePoint::getDelta));
		this.total = new BigDecimal(diff / n / 255.0);
		this.normalized = new BigDecimal(total.doubleValue() * 100);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("ComputedDiff{");
		sb.append("diff=").append(diff);
		sb.append(", total=").append(total);
//		sb.append(", points=").append(points);
		sb.append(", normalized=").append(normalized);
		sb.append('}');
		return sb.toString();
	}

	public double getTotal() {
		return total.setScale(scale.get(), roundingMode).doubleValue();
	}
}
