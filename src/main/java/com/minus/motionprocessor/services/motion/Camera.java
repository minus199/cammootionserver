package com.minus.motionprocessor.services.motion;


import org.opencv.videoio.VideoCapture;
import com.minus.motionprocessor.services.motion.analysisengine.*;
/**
 * Created by minus on 10/20/16.
 */
public class Camera extends Thread {
	public static final String STREAM_URL = "rtsp://192.168.1.5:554/ch0_0.h264";
	private final Engine e;

	private VideoCapture capture;

	public Camera() {
		capture = new VideoCapture();
		e = new Engine(capture);
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		capture.release();
		e.close();
	}

	@Override
	public void run() {
		this.capture.open(STREAM_URL);

		if (this.capture.isOpened()) {
			System.out.println("Connection to camera established.");
			e.ignition();
		}
	}

	public void dispose() {
		try {
			e.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		capture.release();
	}
}
