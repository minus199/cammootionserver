package com.minus.motionprocessor.services.motion.analysis;

/**
 * Created by minus on 10/18/16.
 */

import com.minus.motionprocessor.utils.ImgUtils;
import org.opencv.core.Mat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class ImgDiff {
	private static final Logger logger = LoggerFactory.getLogger(Engine.class);

	public static Optional<ComputedDiff> diff(BufferedImage imageOne, Mat imageTwo) {
		if (imageOne == null || imageTwo == null) return Optional.empty();

		BufferedImage bufferedTwo = ImgUtils.toBufferedImageArrayCopy(imageTwo);

		if (imageOne.equals(bufferedTwo)) return Optional.empty();

		return diff(imageOne, bufferedTwo);
	}

	public static double newDiff(BufferedImage imageOne, BufferedImage imageTwo) {
		double average = DoubleStream.iterate(0, x -> x + 1)
				.limit(imageOne.getWidth() - 1)
				.distinct().
						map(x -> DoubleStream.iterate(0, y -> y + 1)
								.limit(imageOne.getHeight() - 1)
								.distinct()
								.map(y -> {
									int rgb1 = imageOne.getRGB((int) x, (int) y);
									int r1 = (rgb1 >> 16) & 0xff;
									int g1 = (rgb1 >> 8) & 0xff;
									int b1 = (rgb1) & 0xff;

									int rgb2 = imageTwo.getRGB((int) x, (int) y);
									int r2 = (rgb2 >> 16) & 0xff;
									int g2 = (rgb2 >> 8) & 0xff;
									int b2 = (rgb2) & 0xff;

									return Math.abs(r1 - r2) + Math.abs(g1 - g2) + Math.abs(b1 - b2);
								}).sum()).sum();

		return ((average) / 255 / (imageOne.getHeight() * imageOne.getWidth()));
	}

	private static Optional<ComputedDiff> diff(BufferedImage imageOne, BufferedImage imageTwo) {
		int height = imageOne.getHeight() - 1;
		int width = imageOne.getWidth() - 1;
		List<ObscurePoint> points = IntStream.range(0, height)
				.mapToObj(y -> IntStream.range(0, width)
						.mapToObj(x -> extractDeltaPixels(imageOne, imageTwo, y, x)))
				.flatMap(pS -> pS)
				.collect(Collectors.toList());

		points.stream().collect(Collectors.summingInt(ObscurePoint::getDelta));
		return Optional.of(new ComputedDiff(width, height, points));
	}

	private static ObscurePoint extractDeltaPixels(BufferedImage imageOne, BufferedImage imageTwo, int y, int x) {
		int rgb1 = imageOne.getRGB(x, y);
		int rgb2 = imageTwo.getRGB(x, y);

		int r1 = (rgb1 >> 16) & 0xff;
		int r2 = (rgb2 >> 16) & 0xff;
		int r = Math.abs(r1 - r2);

		int g1 = (rgb1 >> 8) & 0xff;
		int g2 = (rgb2 >> 8) & 0xff;
		int g = Math.abs(g1 - g2);

		int b1 = (rgb1) & 0xff;
		int b2 = (rgb2) & 0xff;
		int b = Math.abs(b1 - b2);

		return new ObscurePoint(x, y, r, g, b);
	}
}
