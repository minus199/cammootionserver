package com.minus.motionprocessor.services.motion.analysisengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * Created by minus on 10/30/16.
 */
public class Directories {
	private static final Logger logger = LoggerFactory.getLogger(Directories.class);
	private static final Path baseMotionCaptureDirectory = Paths.get("/mnt/47D93A6444EC43DB/motion/");

	private final static Path longEventDirectory;
	//	public final static File eventDirectory;
	private final static Path shortEventDirectory;
	private final static Set<PosixFilePermission> perms;
	private final static FileAttribute<Set<PosixFilePermission>> attr;

	static {

		perms = PosixFilePermissions.fromString("rwxr-----");
		attr = PosixFilePermissions.asFileAttribute(perms);

		longEventDirectory = Paths.get(baseMotionCaptureDirectory.toString(), "long");
		shortEventDirectory = Paths.get(baseMotionCaptureDirectory.toString(), "short");

		try {
			Files.createDirectories(longEventDirectory, attr);
			Files.createDirectory(shortEventDirectory, attr);
		} catch (IOException e) {
			logger.error("Error: {}", e);
		}


		/*try {
			Files.createDirectories(longEventDirectory, attr);
			Files.createDirectory(shortEventDirectory, attr);
		} catch (UnsupportedOperationException e1) {
			// if the array contains an attribute that cannot be set atomically when creating the directory
			logger.error("Unable to create file {}, why you ask? {}", e1.getClass().getSimpleName().replaceFirst("Exception", ""));
			System.exit(1);
		} catch (FileAlreadyExistsException e1) {
			logger.error("Unable to create file {}, why you ask? {}", e1.getClass().getSimpleName().replaceFirst("Exception", ""));
		} catch (IOException e1) {
			//if an I/O error occurs or the parent directory does not exist
			logger.error("Unable to create file {}, why you ask? {}", e1.getClass().getSimpleName().replaceFirst("Exception", ""));
			System.exit(1);
		} catch (SecurityException e1) {
			logger.error("Unable to create file {}, why you ask? {}", e1.getClass().getSimpleName().replaceFirst("Exception", ""));
//			throw new RuntimeException("Unable to create in the specified location");
			System.exit(1);
		}*/
	}

	public static File outputFile() {
		Path path = Paths.get(baseMotionCaptureDirectory.toString(),
		                      Paths.get(System.currentTimeMillis() + ".avi").toString());

		return new File(path.toUri());
	}

	public static Path longEventPath() {
		return longEventDirectory;
	}

	public static Path shortEventPath() {
		return shortEventDirectory;
	}
}
