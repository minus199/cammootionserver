package com.minus.motionprocessor.services;

import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static java.util.Arrays.asList;

/*
*
 * Created by minus on 10/6/16.
*/

//@Service
public class UserService /*implements UserDetailsService */{

	/*@Override
	public GoogleUser loadUserByUsername(String username) throws
			UsernameNotFoundException {

		*//*final AuthdataRecord record = Ctx.use(ctx -> ctx.select().from(Tables.AUTHDATA)
				.where(Tables.AUTHDATA.USER_ID.eq(username)).fetchOneInto(AuthdataRecord.class))
				.orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));

		return new GoogleUser(record, getGrantedAuthorities(username));*//*
		throw new UnsupportedOperationException();
	}*/

	private Collection<? extends GrantedAuthority> getGrantedAuthorities(String
			                                                                     username) {
		Collection<? extends GrantedAuthority> authorities;
		if (username.equals("John")) {
			authorities = asList(() -> "ROLE_ADMIN", () -> "ROLE_BASIC");
		} else {
			authorities = asList(() -> "ROLE_BASIC");
		}
		return authorities;
	}

	/*public static class GoogleUser implements UserDetails {
		private final AuthdataRecord record;
		private final Collection<? extends GrantedAuthority> grantedAuthorities;

		public GoogleUser(AuthdataRecord record, Collection<? extends GrantedAuthority> grantedAuthorities) {

			this.record = record;
			this.grantedAuthorities = grantedAuthorities;
		}

		public AuthdataRecord getRecord() {
			return record;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return grantedAuthorities;
		}

		@Override
		public String getPassword() {
			return new String(record.getPassword());
		}

		@Override
		public String getUsername() {
			return record.getUsername();
		}

		@Override
		public boolean isAccountNonExpired() {
			return false;
		}

		@Override
		public boolean isAccountNonLocked() {
			return false;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return false;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
	}*/
}
