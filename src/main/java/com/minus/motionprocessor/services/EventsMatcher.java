package com.minus.motionprocessor.services;

import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.exceptions.EventMismatchException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/2/16.
 */
public class EventsMatcher {
	private final Timestamp timestampFirst;
	private final Timestamp timestampLast;

	private EventsMatcher(File first, File last) {
		timestampFirst = extractCreationDate(first);
		Timestamp timestampLastInBetween = extractCreationDate(last);
		timestampLast = timestampLastInBetween.equals(timestampFirst)
				? new Timestamp(timestampFirst.getTime() + 1000)
				: timestampLastInBetween;
	}

	public static Timestamp convertStringToTimestamp(String str_date, String format) {
		try {
			DateFormat formatter = new SimpleDateFormat(format);
			return new Timestamp(formatter.parse(str_date).getTime());
		} catch (ParseException e) {
			System.out.println("Exception :" + e);
			return null;
		}
	}

	private void fileAttribute(File first) {
		BasicFileAttributes attr = null;
		try {
			attr = Files.readAttributes(first.toPath(), BasicFileAttributes.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("creationTime: " + attr.creationTime());
		System.out.println("lastAccessTime: " + attr.lastAccessTime());
		System.out.println("lastModifiedTime: " + attr.lastModifiedTime());
	}

	private Timestamp extractCreationDate(File file) {
		final String[] split = file.getAbsolutePath().split("/events/[0-9]")[1].split("/");
		final List<String> collect = Stream.of(split).filter(s -> s.matches("[0-9]+")).collect(Collectors.toList());
		final String dateParts = collect.stream().collect(Collectors.joining("-"));

		return convertStringToTimestamp("20" + dateParts, "yyyy-MM-dd-HH-mm-ss"); // normalize for the 2k :D
	}

	public static Stream<Event> match(File first, File last) throws EventMismatchException {
		final EventsMatcher eventsMatcher = new EventsMatcher(first, last);
		return Ctx.use(ctx ->
				ctx.select()
						.from(Tables.EVENTS)
						.where(Tables.EVENTS.STARTTIME.between(eventsMatcher.timestampFirst).and(eventsMatcher.timestampLast))
						.fetch().into(EventsRecord.class))
				.orElseThrow(EventMismatchException::new)
				.stream().map(Event::new);
	}
}
