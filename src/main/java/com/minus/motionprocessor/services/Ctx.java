package com.minus.motionprocessor.services;

import com.minus.motionprocessor.api.exceptions.UserNotFoundException;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultConnectionProvider;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.Optional;

import static java.sql.DriverManager.getConnection;

/**
 * Created by minus on 10/2/16.
 */
public class Ctx {
	public static <T> Optional<T> use(CtxRunnable<T> runnable) {
		try (Connection c = getConnection("jdbc:mysql://localhost/zm", "root", "")) {
			DSLContext ctx = DSL.using(new DefaultConfiguration()
					.set(new DefaultConnectionProvider(c))
					.set(SQLDialect.MYSQL)
					.set(new Settings().withExecuteLogging(false)));

			return Optional.ofNullable(runnable.use(ctx));
		} catch (Exception e) {
			e.printStackTrace();
			return Optional.empty();
		}
	}

	public interface CtxRunnable<T> {
		T use(DSLContext ctx);
	}
}
