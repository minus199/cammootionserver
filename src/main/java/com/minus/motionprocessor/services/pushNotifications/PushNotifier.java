package com.minus.motionprocessor.services.pushNotifications;

import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.exceptions.EmptyDataException;

import java.util.List;

/**
 * Created by minus on 10/4/16.
 */
abstract class PushNotifier<T, C> implements AutoCloseable {
	protected static <T extends PushNotifier<List<Event>, ?>> boolean validateData(String data) throws EmptyDataException {

		if (data.isEmpty()) {
			throw new EmptyDataException();
		}
		return false;
	}

	abstract boolean push(String data);

	abstract boolean push(T dataObject);

	abstract C getClient();
}
