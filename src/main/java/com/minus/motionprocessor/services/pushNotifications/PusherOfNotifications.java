package com.minus.motionprocessor.services.pushNotifications;

import com.minus.motionprocessor.entities.event.Event;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/2/16.
 */
public class PusherOfNotifications {

	public static final Class<PlatformAll> PlatformAll = PlatformAll.class;
	public static final Class<PlatformAndroid> PlatformAndroid = PlatformAndroid.class;

	private PusherOfNotifications() {

	}

	public synchronized static <T extends PushNotifier<List<Event>, ?>> void push(Class<T> platformClass, String data) {
		try {
			platformClass.newInstance().push(data);
			System.out.format("Dispatched: %s on %s\n", data, platformClass.getSimpleName());
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized static <T extends PushNotifier<List<Event>, ?>> void push(Class<T> platformClass, List<Event> events) {
		try {
			if (platformClass.newInstance().push(events))
				System.out.format(
						"Dispatched: %s on %s\n",
						events.stream().map(Event::toJson).collect(Collectors.joining("\n")),
						platformClass.getSimpleName()
				);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized static <T extends PushNotifier<List<Event>, ?>> void push(Class<T> platformClass, Stream<Event> eventStream) {
		push(platformClass, eventStream.collect(Collectors.toList()));
	}
}
