package com.minus.motionprocessor.services.pushNotifications;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.entities.event.EventPriority;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by minus on 10/4/16.
 */
class PlatformAll extends PushNotifier<List<Event>, CloseableHttpClient> {

	private static final CloseableHttpClient httpclient = HttpClients.createDefault();
	private final List<NameValuePair> formParams = new ArrayList<>();

	@Override
	boolean push(String data) {
		formParams.add(new BasicNameValuePair("token", "azvgd3i1kzqg4yerphoudj5nm5sva3"));
		formParams.add(new BasicNameValuePair("user", "uwkgvd87aa7c3kcgvnk8hdf8qz1ej3"));
		formParams.add(new BasicNameValuePair("message", data));
		formParams.add(new BasicNameValuePair("priority", EventPriority.Normal_Priority.asCode()));
		return !execute().toString().isEmpty();
	}

	@Override
	public boolean push(List<Event> dataObject) {
		final String eventMsg = dataObject.stream().map(Event::toJson).collect(Collectors.joining("\n"));
		return push(eventMsg);
//		dataObject.stream().max((o1, o2) -> o1.getPriority().compareTo(o2.getPriority()));
	}

	private JsonObject execute() {
		ResponseHandler<JsonObject> rh = handleResponse();

		try {
			return httpclient.execute(produceEntity(), rh);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private HttpPost produceEntity() {
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, Consts.UTF_8);
		HttpPost httppost = new HttpPost("https://api.pushover.net/1/messages");
		httppost.setEntity(entity);

		return httppost;
	}

	private ResponseHandler<JsonObject> handleResponse() {
		return response -> {
			StatusLine statusLine = response.getStatusLine();
			HttpEntity entity = response.getEntity();

			java.util.Scanner s = new java.util.Scanner(entity.getContent()).useDelimiter("\\A");
			if (statusLine.getStatusCode() >= 300) {
				throw new HttpResponseException(
						statusLine.getStatusCode(),
						statusLine.getReasonPhrase() + (s.hasNext() ? s.next() : ""));
			}

			Gson gson = new GsonBuilder().create();
			Reader reader = new InputStreamReader(entity.getContent(), ContentType.getOrDefault(entity).getCharset());
			return gson.fromJson(reader, JsonObject.class);
		};
	}

	@Override
	public void close() throws Exception {
		httpclient.close();
	}

	@Override
	CloseableHttpClient getClient() {
		return httpclient;
	}
}
