package com.minus.motionprocessor.services.pushNotifications;

import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.exceptions.EmptyDataException;
import com.urbanairship.api.client.Response;
import com.urbanairship.api.client.UrbanAirshipClient;
import com.urbanairship.api.push.PushRequest;
import com.urbanairship.api.push.model.DeviceType;
import com.urbanairship.api.push.model.DeviceTypeData;
import com.urbanairship.api.push.model.PushPayload;
import com.urbanairship.api.push.model.audience.Selectors;
import com.urbanairship.api.push.model.notification.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by minus on 10/3/16.
 */
class PlatformAndroid extends PushNotifier<List<Event>, UrbanAirshipClient> {

	private static final Logger logger = LoggerFactory.getLogger(PlatformAndroid.class);

	final private static String appKey = "N9NWG1KiSG2VLLXX4VPh1Q";
	final private static String appSecret = "LU4Ol0txRRWBbR77mBYtVg";

	private final UrbanAirshipClient client = UrbanAirshipClient.newBuilder()
			.setKey(appKey)
			.setSecret(appSecret)
			.build();

	@Override
	public boolean push(List<Event> eventData) {
		final String eventMsg = eventData.stream().map(Event::toJson).collect(Collectors.joining("\n"));
		try {
			validateData(eventMsg);

			return push(eventMsg);
		} catch (EmptyDataException e) {
			return false;
		}
	}

	@Override
	UrbanAirshipClient getClient() {
		return client;
	}

	@Override
	final public boolean push(String data) {
		PushPayload payload = PushPayload.newBuilder()
				.setAudience(Selectors.all())
				.setNotification(Notifications.alert(data))
				.setDeviceTypes(DeviceTypeData.of(DeviceType.ANDROID))
				.build();

		PushRequest request = PushRequest.newRequest(payload);

		logger.debug("Pushing notification with the following payload: %s", data);
		try {
			Response response = client.execute(request);
			logger.debug(String.format("Response %s", response.toString()));
			return true;
		} catch (IOException e) {
			logger.error("IOException in API request " + e.getMessage());
		} finally {
			try {
				close();
			} catch (Exception e) {

			}
		}
		return false;
	}

	@Override
	public void close() throws Exception {
		client.close();
	}
}
