package com.minus.motionprocessor.services.encryption;

import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * Created by minus on 10/5/16.
 */
@Service
public class CipherManager {
	public static final String ALGORITHM = "RSA";
	private byte[] iv;

	public void generateKeyPair(AuthdataRecord AuthdataRecord) {
		KeyPairGenerator kpg;
		KeyFactory fact;
		try {
			kpg = KeyPairGenerator.getInstance(ALGORITHM);
			fact = KeyFactory.getInstance(ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}

		kpg.initialize(2048);
		KeyPair kp = kpg.genKeyPair();

		save(kp, fact, AuthdataRecord);
	}

	public byte[] decrypt(byte[] data, AuthdataRecord record) {
		PrivateKey privateKey = loadPrivate(record);
		return cipher(data, privateKey, Cipher.DECRYPT_MODE);
	}

	public byte[] encrypt(byte[] data, AuthdataRecord usersRecord) {
		PublicKey pubKey = loadPublic(usersRecord);
		return cipher(data, pubKey, Cipher.ENCRYPT_MODE);
	}

	private byte[] cipher(byte[] data, Key key, int mode) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			/*final IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
			if (mode == Cipher.ENCRYPT_MODE) {
				this.iv = cipher.getIV();
			}*/

			cipher.init(mode, key/*, ivParameterSpec*/);

			return cipher.doFinal(data);
		} catch (/*InvalidAlgorithmParameterException | */IllegalBlockSizeException
				| BadPaddingException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new RuntimeException(e);
		}


		/*SecretKeySpec skeySpec = new SecretKeySpec(pubKey.getEncoded(), "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

		if (mode == Cipher.ENCRYPT_MODE) {
			cipher.init(mode, skeySpec, generateIV());
			iv = cipher.getIV();
		}

		if (mode == Cipher.DECRYPT_MODE) {
			cipher.init(mode, skeySpec, new IvParameterSpec(getIV()));
		}

		return cipher;*/

	}

	private void save(KeyPair kp, KeyFactory fact, AuthdataRecord AuthdataRecord) {

		try {
			RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(), RSAPublicKeySpec.class);
			AuthdataRecord.setPublicexponent(pub.getPublicExponent().toByteArray());
			AuthdataRecord.setPublicmodulus(pub.getModulus().toByteArray());

			RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(), RSAPrivateKeySpec.class);
			AuthdataRecord.setPrivateexponent(priv.getPrivateExponent().toByteArray());
			AuthdataRecord.setPrivatemodulus(priv.getModulus().toByteArray());

//			AuthdataRecord.setIv(generateIV().getIV());

			/*ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(pub.getPublicExponent().toByteArray());
			outputStream.write(pub.getModulus().toByteArray());*/
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	private PublicKey loadPublic(AuthdataRecord userRecord) {
		final BigInteger m = new BigInteger(userRecord.getPublicmodulus());
		final BigInteger e = new BigInteger(userRecord.getPublicexponent());
		RSAPublicKeySpec keySpec = new RSAPublicKeySpec(m, e);

		try {
			KeyFactory fact = KeyFactory.getInstance(ALGORITHM);
			return fact.generatePublic(keySpec);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException e1) {
			throw new RuntimeException(e1);
		}
	}

	private PrivateKey loadPrivate(AuthdataRecord userRecord) {
		final BigInteger m = new BigInteger(userRecord.getPrivatemodulus());
		final BigInteger e = new BigInteger(userRecord.getPrivateexponent());
		final RSAPrivateKeySpec privateKeySpec = new RSAPrivateKeySpec(m, e);

		try {
			KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
			return keyFactory.generatePrivate(privateKeySpec);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
			throw new RuntimeException(ex);
		}
	}

	private IvParameterSpec generateIV() {
		SecureRandom random = new SecureRandom();
		byte[] iv = new byte[16];
		random.nextBytes(iv);

		return new IvParameterSpec(iv);
	}
}
