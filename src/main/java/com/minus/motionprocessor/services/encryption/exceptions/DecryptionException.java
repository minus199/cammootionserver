package com.minus.motionprocessor.services.encryption.exceptions;

/**
 * Created by minus on 10/5/16.
 */
public class DecryptionException extends Exception {
	public DecryptionException(Exception e) {
		super (DecryptionException.class.getName(), e);
	}
}
