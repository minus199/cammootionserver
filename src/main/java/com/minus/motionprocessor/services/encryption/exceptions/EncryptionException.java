package com.minus.motionprocessor.services.encryption.exceptions;

/**
 * Created by minus on 10/5/16.
 */
public class EncryptionException extends Exception {
	public EncryptionException(Exception e) {
		super(EncryptionException.class.getName(), e);
	}
}
