package com.minus.motionprocessor.services.sysadmin;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by minus on 10/13/16.
 */

@Service
public class SystemAdminService {
	private final Runtime runtime;

	public SystemAdminService() {
		runtime = Runtime.getRuntime();
	}

	// /usr/local/bin/noip2 -c /etc/noip2.conf

	public String restartService(String serviceName) {
		return execCmd(String.format("service %s restart", serviceName));
	}

	public String getServiceStatus(String serviceName) {
		return execCmd(String.format("service %s status", serviceName));
	}

	public String startService(String serviceName) {
		return execCmd(String.format("service %s start", serviceName));
	}

	public String stopService(String serviceName) {
		return execCmd(String.format("service %s stop", serviceName));
	}

	public Integer getProcessID(String serviceName) throws IOException {
		return Integer.valueOf(execCmd(String.format("pidof %s", serviceName).trim()));
//		return Integer.valueOf(execCmd("./src/main/resources/scripts/getProcessID.sh").trim());
	}

	public void turnOffLed(){
//		execCmd();
	}

	public void truncateEvents() {

	}

	private String execCmd(String cmd) {
		try {
			final Process proc = runtime.exec(cmd);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String output = "";
			String s;
			while ((s = stdInput.readLine()) != null) {
				output += s;
			}

			return output;
		} catch (Exception e) {
			throw new RuntimeException("unable to run command", e);
		}
	}
}
