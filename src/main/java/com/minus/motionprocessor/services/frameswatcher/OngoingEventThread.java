package com.minus.motionprocessor.services.frameswatcher;

/**
 * Created by minus on 10/13/16.
 */
public class OngoingEventThread implements Runnable {
	private boolean paused = false;
	private final Object LOCK = new Object();

	public void run() {
		while (true) {
			synchronized(LOCK) {
				final long startTime = System.currentTimeMillis();
				while(System.currentTimeMillis() - startTime < 5){
					try {
						System.out.println("waiting");
						LOCK.wait();
					} catch (InterruptedException e) {
						System.out.println("woke up" + (System.currentTimeMillis() - startTime ));
					}

				}

			}
			/*try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		}
	}

	public void pause() {
		synchronized(LOCK) {
			paused = true;
			LOCK.notifyAll();
		}
	}

	public void resume() {
		synchronized(LOCK) {
			paused = false;
			LOCK.notifyAll();
		}
	}
}