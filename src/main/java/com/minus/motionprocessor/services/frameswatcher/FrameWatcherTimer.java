package com.minus.motionprocessor.services.frameswatcher;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by minus on 10/14/16.
 */
public class FrameWatcherTimer {
	private final StartPredicate resetPredicate;
	private final OnTimeExceededFunction exceededFunction;
	private long maxWaitingTime;
	private double factor = 1.0;
	private String startTime;
	private long startTimeLong;
	private Thread timerExceededWorkerThread;

	public FrameWatcherTimer(StartPredicate startPredicate, OnTimeExceededFunction exceededFunction, long maxWaitingTime) {
		this.resetPredicate = startPredicate;
		this.exceededFunction = exceededFunction;
		this.maxWaitingTime = maxWaitingTime;
		resetTimer();
	}

	long getElapsed() {
		return System.currentTimeMillis() - startTimeLong;
	}

	/**
	 * @param value to be compared with
	 * @return 1 if current elapsing time is greater than value. 0 if equals. -1 if value is larger.
	 */
	int compareElapsed(long value) {
		return getElapsed() > value ? 1 : (getElapsed() < value ? - 1 : 0);
	}

	public FrameWatcherTimer testReset() {
		if (resetPredicate.test()) {
			resetTimer();
		}

		if (isMaxTimeExceeded()) {
			exceededFunction.accept(this);
			factor = 1;
			resetTimer();
		}

//		System.out.format("Started %s | Elapsing: %d\n", startTime, getElapsed());

		return this;
	}

	boolean isMaxTimeExceeded() {
		return getElapsed() > maxWaitingTime;
	}

	private FrameWatcherTimer resetTimer() {
		startTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
		startTimeLong = System.currentTimeMillis();
		maxWaitingTime /= factor;
		factor -= factor > 0.2 ? 0.1 : 0;
		timerExceededWorkerThread = new Thread(this::testReset);
		return this;
	}

	public interface OnTimeExceededFunction {
		void accept(FrameWatcherTimer timer);
	}

	public interface StartPredicate {
		boolean test();
	}
}
