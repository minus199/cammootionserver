package com.minus.motionprocessor.services.frameswatcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by minus on 10/3/16.
 */
class DirectoryRegistrar {
	private final WatchService watcher;
	private final Map<WatchKey, Path> keys;

	DirectoryRegistrar(Path dir, WatchService watcher) {
		this.watcher = watcher;
		this.keys = new HashMap<>();

		System.out.format("Scanning %s ...\n", dir);
		try {
			registerAll(dir);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
		System.out.println("Done.");
	}

	final Path isWatched(WatchKey key) {
		Path dir = keys.get(key);
		if (dir == null) {
			throw new RuntimeException("WatchKey not recognized!!");
		}

		return dir;
	}

	final boolean isEmptyAfter(WatchKey key) {
		boolean valid = key.reset();
		if (! valid) {
			keys.remove(key);
			if (keys.isEmpty()) {
				return true;
			}
		}

		return false;
	}

	private void register(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		Path prev = keys.get(key);

		if (prev == null) {
			System.out.format("register: %s\n", dir);
		} else {
			if (! dir.equals(prev)) {
				System.out.format("update: %s -> %s\n", prev, dir);
			}
		}

		keys.put(key, dir);
	}

	private void registerAll(final Path start) throws IOException {
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
					throws IOException {
				register(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	void registerDir(File file) {
		try {
			if (Files.isDirectory(file.toPath(), NOFOLLOW_LINKS)) {
				registerAll(file.toPath());
			}
		} catch (IOException x) {
			throw new RuntimeException(x);
		}
	}
}
