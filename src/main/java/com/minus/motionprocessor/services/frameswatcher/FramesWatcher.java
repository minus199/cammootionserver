package com.minus.motionprocessor.services.frameswatcher;

import com.minus.motionprocessor.entities.Callbacks;
import com.minus.motionprocessor.tasks.PushTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

public class FramesWatcher {
	private static final Logger logger = LoggerFactory.getLogger(FramesWatcher.class);
	private final Timer timer = new Timer(true);
	private final ConcurrentLinkedDeque<File> files = new ConcurrentLinkedDeque<>();
	private final WatchService watcher;
	private final DirectoryRegistrar directoryRegistrar;
	private final FrameWatcherTimer frameWatcherTimer;
	private final ExecutorService executorService;

	private TimerTask timerTask;

	private FramesWatcher(Path dir) {
		try {
			this.watcher = FileSystems.getDefault().newWatchService();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		executorService = Executors.newFixedThreadPool(1);
		directoryRegistrar = new DirectoryRegistrar(dir, watcher);
		frameWatcherTimer = new FrameWatcherTimer(
				() -> timerTask == null,
				t -> {
					new Thread(() -> {
//						timer.schedule(new PushTask(files), 0);
/*
						String payload = Ctx.use(ctx
								-> ctx.select()
								.from(Tables.EVENTS)
								.where(Tables.EVENTS.ID.eq(ctx
										.select(DSL.max(Tables.EVENTS.ID)).from(Tables.EVENTS))).fetchOne().into(Events.class))
								.map(Events::toString).orElse("Shit's still in progress.");*/

//						PusherOfNotifications.push(PusherOfNotifications.PlatformAndroid, "Shit's still in progress.");
					}).start();
				},
				7000L);
	}

	public static void listen() {
		new Thread(InstanceContainer.instance::processEvents).start();
	}

	private WatchKey waitForFrame() {
		logger.debug("Waiting for new frames.");
		try {
			return watcher.take();
		} catch (InterruptedException x) {
			Thread.currentThread().interrupt();
			throw new RuntimeException(x);
		}
	}

	private void processEvents() {
		while (! Thread.currentThread().isInterrupted()) {
			WatchKey key = waitForFrame();
			logger.debug("New frame accepted");
			frameWatcherTimer.testReset(); // is task null reset
			List<File> files = normalizeEventFrames(key, directoryRegistrar.isWatched(key));
			this.files.addAll(files);
			logger.debug(__("Added %d new files", files.size()));
			if (timerTask != null) {
				timerTask.cancel();
			}

			timerTask = new PushTask(this.files, new Callbacks().setExit(() -> {
				this.files.clear();
				return Optional.of(true);
			}));

			timer.schedule(timerTask, 1500);

			if (directoryRegistrar.isEmptyAfter(key)) {
				break;
			}
		}
	}

	private List<File> normalizeEventFrames(WatchKey key, Path dir) {
		return key.pollEvents().stream()
				.filter(watchEvent -> watchEvent.kind().equals(ENTRY_CREATE))
				.parallel()
				.map(watchEvent -> {
					Path name = (Path) watchEvent.context();
					final File file = new File(dir.resolve(name).toUri());
					directoryRegistrar.registerDir(file);
					return file;
				})
				.filter(file -> {
					try {
						return file.isFile() && "jpg".equals(file.getName().split("\\.")[1]);
					} catch (IndexOutOfBoundsException e) {
						return false;
					}
				}).distinct().collect(Collectors.toList());
	}

	private String __(String message, Object... args) {
		return String.format(message, (Object[]) args);
	}

	private static final class InstanceContainer {
		private static final FramesWatcher instance = new FramesWatcher(Paths.get("/mnt/47D93A6444EC43DB"));
	}
}