package com.minus.motionprocessor.utils;

import java.math.BigDecimal;

/**
 * Created by minus on 10/22/16.
 */
public class NumberUtils {
	public static double precision(double val, int percision){
		return BigDecimal.valueOf(val).setScale(percision, BigDecimal.ROUND_UP).doubleValue();
	}
}
