package com.minus.motionprocessor.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/21/16.
 */
public class ImgUtils {
	private static final String imgLocation = "/mnt/47D93A6444EC43DB/motion/";
	private final static File longEventDirectory;
	//	public final static File eventDirectory;
	private final static File shortEventDirectory;

	static {
		try {
			longEventDirectory = new File(imgLocation + "long");
			longEventDirectory.mkdirs();
			shortEventDirectory = new File(imgLocation + "short");
			shortEventDirectory.mkdirs();
		} catch (Exception e) {
			throw new RuntimeException("Could not init class.", e);
		}
	}

	public static BufferedImage matToBufferedImage(Mat matBGR) {
		int width = matBGR.width(), height = matBGR.height(), channels = matBGR.channels();
		byte[] sourcePixels = new byte[width * height * channels];
		matBGR.get(0, 0, sourcePixels);
		BufferedImage image = new BufferedImage(width, height, matBGR.channels() > 1 ? BufferedImage.TYPE_3BYTE_BGR : BufferedImage.TYPE_BYTE_GRAY);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(sourcePixels, 0, targetPixels, 0, sourcePixels.length);
		return image;
	}

	public static Image bufferedToImage(BufferedImage frame) {
		return SwingFXUtils.toFXImage(frame, null);
	}

	public static Mat gray(Mat matToShow) {
		// convert the image to gray scale
		Imgproc.cvtColor(matToShow, matToShow, Imgproc.COLOR_BGR2GRAY);
		return matToShow;
	}

	public static void contours(Mat benchmarkMat, Mat currentMat) {
		Mat movement = new Mat();
		Mat hierarchy = new Mat();

		Imgproc.cvtColor(currentMat, currentMat, Imgproc.COLOR_BGR2GRAY);

		Core.absdiff(benchmarkMat, currentMat, movement);
		Imgproc.threshold(movement, movement, 10, 255, Imgproc.THRESH_BINARY);
		final List<MatOfPoint> contours = new ArrayList<>();
		Imgproc.findContours(movement, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_KCOS);

		if (hierarchy.size().height > 0 && hierarchy.size().width > 0) {
			// for each contour, display it in blue
			for (int idx = 0; idx >= 0; idx = (int) hierarchy.get(0, idx)[0]) {
				Imgproc.drawContours(currentMat, contours, idx, new Scalar(250, 0, 0));
			}
		}
	}

	public static BufferedImage toBufferedImageArrayCopy(Mat m) {
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;
	}

	public static java.awt.Image getImageFromArray(int[] pixels, int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		WritableRaster raster = (WritableRaster) image.getData();

		raster.setPixels(0, 0, width, height, pixels);
		return image;
	}

	public static Path imgPath(String... args) {
		String d = "-";
		String argsConcat = args.length > 0 ? d + Stream.of(args).collect(Collectors.joining(d)) : "";

		return new File(imgLocation + System.currentTimeMillis() + argsConcat + ".png").toPath();
	}

	public static File motionCaptureDirectory(boolean isShort) {
		String s = (isShort ? getShortEventDirectory() : getLongEventDirectory()).getPath()
		           + File.separator
		           + System.currentTimeMillis()
		           + ".avi";


		File file = new File(s);
//		file.mkdirs();

		return file;
	}

	public static File getLongEventDirectory() {
		return longEventDirectory;
	}

	public static File getShortEventDirectory() {
		return shortEventDirectory;
	}
}
