package com.minus.motionprocessor.utils;

import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import org.apache.commons.codec.binary.Hex;
import org.springframework.web.bind.annotation.RequestParam;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by minus on 10/9/16.
 */
public class CipherUtils {

	public final static char INVISIBLE_DELIMITER = 0x00002063;

	public static String generateAccessToken(AuthdataRecord authdataRecord) {
		final String undigestedAccessToken = new String(Base64.getEncoder().encode(authdataRecord.getPublicexponent()))
				.concat(String.valueOf(CipherUtils.INVISIBLE_DELIMITER))
				.concat(new String(Base64.getEncoder().encode(authdataRecord.getPublicmodulus())));

		byte[] undigestedAccessTokenBytes = undigestedAccessToken.getBytes(StandardCharsets.UTF_8);
		return digest(undigestedAccessTokenBytes);
	}

	private static String digest(byte[] undigestedAccessTokenBytes) {
		try {
			return new String(Hex.encodeHex(MessageDigest.getInstance("MD5").digest(undigestedAccessTokenBytes)));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static String hashPassword(@RequestParam("password") String password) {
		return digest(password.getBytes());
	}
}
