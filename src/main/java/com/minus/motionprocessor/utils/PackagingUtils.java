package com.minus.motionprocessor.utils;

import com.minus.motionprocessor.contracts.Mappable;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/5/16.
 */
public class PackagingUtils {

	public static <T> Map<String, ?> toMap(T obj) {
		return Stream.of(obj.getClass().getMethods())
				.filter(method -> {
					final String name = StringUtils.defaultIfEmpty(method.getName(), "");
					return "get".equals(name.substring(0, 3));
				})
				.collect(Collectors.toMap(
						o -> StringUtils.uncapitalize(o.getName().substring(3, o.getName().length())),
						t -> {
							try {
								final Object invoke = t.invoke(obj);
								if (invoke instanceof Mappable) {
									return ((Mappable) invoke).toMap();
								}

								return invoke;
							} catch (IllegalAccessException | InvocationTargetException | NullPointerException e) {
								return "";
							}
						}));
	}
}
