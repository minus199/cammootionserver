package com.minus.motionprocessor;

/**
 * Created by minus on 10/5/16.
 */

import com.minus.motionprocessor.services.motion.Camera;
import org.opencv.core.Core;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("application.properties")
public class App {
	private static final Logger logger = LoggerFactory.getLogger(App.class);

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);

		Camera camera = new Camera();
		camera.start();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				System.out.println("Im done");
				camera.dispose();
			}
		});
	}
}