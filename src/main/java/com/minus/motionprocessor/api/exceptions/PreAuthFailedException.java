package com.minus.motionprocessor.api.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by minus on 10/9/16.
 */
public class PreAuthFailedException extends AuthenticationException{
	public PreAuthFailedException() {
		super("PreAuth Failed. Your credentials are not allowed here. Maybe try somewhere else?");
	}
}
