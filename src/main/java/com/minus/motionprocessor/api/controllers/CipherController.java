package com.minus.motionprocessor.api.controllers;

import com.minus.motionprocessor.api.Response;
import com.minus.motionprocessor.api.exceptions.UserNotFoundException;
import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import com.minus.motionprocessor.services.Ctx;
import com.minus.motionprocessor.services.encryption.CipherManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Created by minus on 10/9/16.
 */
@RestController
public class CipherController {
	private final CipherManager cipherManager;

	@Autowired
	public CipherController(CipherManager cipherManager) {
		this.cipherManager = cipherManager;
	}

	@RequestMapping(value = "/api/cipher/e")
	public Response encrypt(@RequestParam String data) throws UserNotFoundException {
		final byte[] encrypt = cipherManager.encrypt(data.getBytes(StandardCharsets.UTF_8), fetchUser());
		byte[] base64encode = Base64.getEncoder().encode(encrypt);

		return new Response(HttpStatus.OK, "").addAttribute("encrypted", new String(base64encode));
	}

	@RequestMapping(value = "/api/cipher/d")
	public Response decrypt(@RequestParam String data) throws UserNotFoundException {
		final String urlEncodeAdjust = data.replaceAll(" ", "+");

		byte[] decode = Base64.getDecoder().decode(urlEncodeAdjust.getBytes(StandardCharsets.UTF_8));
		final byte[] decrypt = cipherManager.decrypt(decode, fetchUser());
		return new Response(HttpStatus.OK, new String(decrypt));
	}

	private AuthdataRecord fetchUser() throws UserNotFoundException {
		return Ctx.use(ctx -> ctx.select().from(Tables.AUTHDATA).limit(1).fetchOneInto(AuthdataRecord.class))
				.orElseThrow(UserNotFoundException::new);
	}
}
