package com.minus.motionprocessor.api.controllers;

import com.minus.motionprocessor.api.Response;
import com.minus.motionprocessor.api.controllers.exceptions.NoDataException;
import com.minus.motionprocessor.api.controllers.exceptions.UnsupportedFieldException;
import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.services.Ctx;
import org.jooq.DataType;
import org.jooq.TableField;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by minus on 10/9/16.
 */
@RestController("/api/events")
public class EventsController {
	private final static Map<String, TableField> sorting = new HashMap<>();

	static {
		sorting.put("tot_score", Tables.EVENTS.TOTSCORE);
		sorting.put("alarm_frames", Tables.EVENTS.ALARMFRAMES);
		sorting.put("frames", Tables.EVENTS.FRAMES);
		sorting.put("archived", Tables.EVENTS.ARCHIVED);
		sorting.put("avg_score", Tables.EVENTS.AVGSCORE);
		sorting.put("cause", Tables.EVENTS.CAUSE);
		sorting.put("emailed", Tables.EVENTS.EMAILED);
		sorting.put("end_time", Tables.EVENTS.ENDTIME);
		sorting.put("executed", Tables.EVENTS.EXECUTED);
		sorting.put("length", Tables.EVENTS.LENGTH);
		sorting.put("max_score", Tables.EVENTS.MAXSCORE);
		sorting.put("messaged", Tables.EVENTS.MESSAGED);
		sorting.put("start_time", Tables.EVENTS.STARTTIME);
		sorting.put("uploaded", Tables.EVENTS.UPLOADED);
		sorting.put("videoed", Tables.EVENTS.VIDEOED);
		sorting.put("height", Tables.EVENTS.HEIGHT);
		sorting.put("width", Tables.EVENTS.WIDTH);
	}

	@RequestMapping
	public Response getEvents(@RequestParam(required = false) String startTime) throws NoDataException {
		Date parse;
		try {
			parse = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(startTime);
		} catch (Exception e) {
			parse = new Date();
		}

		final Timestamp startTimestamp = new Timestamp(parse.getTime());

		final List<Event> eventList = Ctx
				.use(ctx -> ctx.select().from(Tables.EVENTS)
						.orderBy(Tables.EVENTS.STARTTIME.desc())
						.seek(startTimestamp)
						.limit(1000).fetch().into(EventsRecord.class))
				.orElseThrow(NoDataException::new)
				.stream().map(Event::new).collect(Collectors.toList());

		return new Response(HttpStatus.OK, "Your events").addAttribute("events", eventList);
	}

	public void getEvent(@RequestParam String by, @RequestParam Object value) throws UnsupportedFieldException {

		final TableField filterBy = sorting.get(by);
		if (filterBy == null) {
			throw new UnsupportedFieldException();
		}
		final DataType dataType = filterBy.getDataType();

		Ctx.use(ctx -> ctx.select().from(Tables.EVENTS).where(filterBy.eq(value)));
	}
}
