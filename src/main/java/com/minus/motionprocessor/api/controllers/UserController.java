package com.minus.motionprocessor.api.controllers;

/**
 * Created by minus on 10/5/16.
 */

import com.minus.motionprocessor.api.Response;
import com.minus.motionprocessor.api.exceptions.PreAuthFailedException;
import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.enums.*;
import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import com.minus.motionprocessor.db.mapping.tables.records.UsersRecord;
import com.minus.motionprocessor.entities.user.UserPojoSimple;
import com.minus.motionprocessor.exceptions.AuthRecordNotFoundException;
import com.minus.motionprocessor.services.Ctx;
import com.minus.motionprocessor.services.encryption.CipherManager;
import com.minus.motionprocessor.utils.CipherUtils;
import org.jooq.DSLContext;
import org.jooq.types.UByte;
import org.omg.CORBA.TRANSACTION_ROLLEDBACK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Locale;
import java.util.Optional;

@RestController(value = "/api/user")
public class UserController {
	private final CipherManager cipherManager;

	@Autowired
	public UserController(CipherManager cipherManager) {
		this.cipherManager = cipherManager;
	}

	@RequestMapping(method = RequestMethod.POST)
	public Response create(@RequestBody UserPojoSimple user) throws AuthenticationException {
		final String accessToken = Ctx.use(ctx -> {
			final AuthdataRecord preAuth = preAuth(user, ctx);
			final AuthdataRecord authdataRecord = preAuth.getId() == null ? create(user, ctx) : preAuth;
			return CipherUtils.generateAccessToken(authdataRecord);
		}).orElseThrow(AuthRecordNotFoundException::new);

		return new Response(HttpStatus.OK, "You are in. Take this -> ").addAttribute("e", accessToken);
	}

	private AuthdataRecord preAuth(@RequestBody UserPojoSimple user, DSLContext ctx) {
		return Optional.ofNullable(
				ctx.select()
						.from(Tables.ALLOWEDEMAILS)
						.leftJoin(Tables.USERS).on(Tables.USERS.USERNAME.eq(suggestedUserName(user)))
						.leftJoin(Tables.AUTHDATA).on(Tables.AUTHDATA.USER_ID.eq(Tables.USERS.ID))
						.where(Tables.ALLOWEDEMAILS.EMAIL.eq(user.getEmail()))
						.fetchOne()
		).orElseThrow(PreAuthFailedException::new).into(AuthdataRecord.class);
	}

	private AuthdataRecord create(@RequestBody UserPojoSimple user, DSLContext ctx) {
		return ctx.transactionResult(configuration -> {
			UsersRecord usersRecord = pojoToRecord(user, ctx);

			usersRecord = ctx.insertInto(Tables.USERS).set(usersRecord)
					.returning().fetchOptional().orElseThrow(TRANSACTION_ROLLEDBACK::new);

			final AuthdataRecord AuthdataRecord = ctx.newRecord(Tables.AUTHDATA)
					.setUserId(usersRecord.getId())
					.setCreated(new Timestamp(System.currentTimeMillis()));
			cipherManager.generateKeyPair(AuthdataRecord);

			return ctx.insertInto(Tables.AUTHDATA).set(AuthdataRecord)
					.returning().fetchOptional().orElseThrow(TRANSACTION_ROLLEDBACK::new);
		});
	}

	private String suggestedUserName(@RequestBody UserPojoSimple user) {
		try {
			return user.getEmail().split("@")[0];
		} catch (IndexOutOfBoundsException ex) {
			throw new BadCredentialsException("Email appears to be invalid.");
		}
	}

	private UsersRecord pojoToRecord(@RequestBody UserPojoSimple user, DSLContext ctx) {
		return ctx.newRecord(Tables.USERS)
				.setUsername(suggestedUserName(user))
				.setPassword(String.valueOf(CipherUtils.hashPassword(user.getEmail())))
				.setControl(UsersControl.View)
				.setDevices(UsersDevices.View)
				.setEnabled(UByte.valueOf(1))
				.setEvents(UsersEvents.View)
				.setGroups(UsersGroups.View)
				.setLanguage(Locale.ENGLISH.getLanguage())
				.setMaxbandwidth("low")
				.setMonitors(UsersMonitors.View)
				.setStream(UsersStream.View)
				.setMonitorids("1")
				.setSystem(UsersSystem.None);
	}
}
