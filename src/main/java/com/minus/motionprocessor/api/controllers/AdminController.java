package com.minus.motionprocessor.api.controllers;

import com.minus.motionprocessor.api.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by minus on 10/13/16.
 */
//@RestController
public class AdminController {
	private static final List<String> allowedProcs = new ArrayList<>(Arrays.asList("apache2", "zoneminder"));


//	@RequestMapping
	public Response resetService(@RequestParam String service) throws InterruptedException, IOException {

		final Process processID = execCmd("ps aux | grep \"/usr/local/bin/noip2\" | grep nobody | awk '{print $2}'");

		InputStreamReader isr = new InputStreamReader(processID.getInputStream());
		BufferedReader br = new BufferedReader(isr);
		String line;
		while ((line = br.readLine()) != null) System.out.println(line);
		processID.waitFor();
/*
		processID.

				Map commands = new HashMap<String, String>();

		"service %s %s"

		""

		Runtime.getRuntime().exec("service %s restart")*/

		return null;
	}

	private Process execCmd(String cmd) {
		try {
			return Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
