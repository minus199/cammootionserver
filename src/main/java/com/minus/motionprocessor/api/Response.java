package com.minus.motionprocessor.api;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by minus on 10/5/16.
 */
public class Response {

	private final HttpStatus status;
	private final String message;
	private final Map<String, Object> data = new HashMap<>();

	public Response(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	public Response addAttribute(String name, Object value) {
		data.put(name, value);
		return this;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Map<String, Object> getData() {
		return data;
	}

	private class Attribute<K, V> {
		private final K key;
		private final V value;

		Attribute(K key, V value) {
			this.key = key;
			this.value = value;
		}

		public K getKey() {
			return key;
		}

		public V getValue() {
			return value;
		}
	}
}