package com.minus.motionprocessor.api.auth;

import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.UsersRecord;
import com.minus.motionprocessor.exceptions.PermissionDeniedException;
import com.minus.motionprocessor.services.Ctx;
import org.jooq.util.mysql.MySQLDSL;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by minus on 10/6/16.
 */
public class AuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
	@Override
	public void commence(final HttpServletRequest request,
	                     final HttpServletResponse response,
	                     final AuthenticationException authException)
			throws IOException, ServletException {

		try {
			final Map<String, String> authData = extractAuthHeader(request);

			Ctx.use(ctx -> ctx.select()
					.from(Tables.USERS)
					.where(Tables.USERS.USERNAME.eq(authData.get("username")))
					.and(Tables.USERS.PASSWORD.eq(MySQLDSL.password(authData.get("password"))))
					.fetchOneInto(UsersRecord.class))
					.orElseThrow(PermissionDeniedException::new);
		} catch (PermissionDeniedException e) {
			authFailed(response, authException);
		}
	}

	private HashMap<String, String> extractAuthHeader(HttpServletRequest request) throws PermissionDeniedException {
		final String authorizationHeader = request.getHeader("authorization");

		if (authorizationHeader != null) {
			try {
				final String[] authorizations = new String(Base64.getDecoder()
						.decode(authorizationHeader.getBytes(StandardCharsets.UTF_8)))
						.split(":", 2);

				final HashMap<String, String> map = new HashMap<>();
				map.put("username", authorizations[0]);
				map.put("password", authorizations[1]);
				return map;
			} catch (Exception e) {
			}
		}

		throw new PermissionDeniedException();
	}

	private void authFailed(HttpServletResponse response, AuthenticationException authException) throws IOException {
		//Authentication failed, send error response.
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");

		PrintWriter writer = response.getWriter();
		writer.println("HTTP Status 401 : " + authException.getMessage());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		setRealmName("MINUS");
		super.afterPropertiesSet();
	}
}
