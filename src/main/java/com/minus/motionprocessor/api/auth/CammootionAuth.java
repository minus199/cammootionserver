package com.minus.motionprocessor.api.auth;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Created by minus on 10/9/16.
 */
public class CammootionAuth extends UsernamePasswordAuthenticationToken{
	public CammootionAuth(Object principal, Object credentials) {
		super(principal, credentials);
	}

	public CammootionAuth(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
	}
}
