package com.minus.motionprocessor.api.auth;

import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.AuthdataRecord;
import com.minus.motionprocessor.db.mapping.tables.records.UsersRecord;
import com.minus.motionprocessor.entities.user.CammootionUser;
import com.minus.motionprocessor.services.Ctx;
import org.jooq.Record;
import org.jooq.util.mysql.MySQLDSL;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by minus on 10/6/16.
 */
@Service
public class AuthProvider extends AbstractUserDetailsAuthenticationProvider {
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
			throws AuthenticationException {
		System.out.println();
	}

	@Override
	protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
		final Record usersRecord = Ctx.use(ctx -> ctx.select()
				.from(Tables.USERS)
				.join(Tables.AUTHDATA).on(Tables.AUTHDATA.USER_ID.eq(Tables.USERS.ID))
				.where(Tables.USERS.USERNAME.eq(usernamePasswordAuthenticationToken.getPrincipal().toString()))
				.and(Tables.USERS.PASSWORD.eq(MySQLDSL.password(usernamePasswordAuthenticationToken.getCredentials().toString())))
				.fetchOne())
				.orElseThrow(() -> new BadCredentialsException("Wrong lawgin, dawg."));

		final CammootionUser cammootionUser = new CammootionUser(
				usersRecord.into(UsersRecord.class),
				usernamePasswordAuthenticationToken.getAuthorities()
		);

		final AuthdataRecord authdataRecord = usersRecord.into(AuthdataRecord.class);
//		usernamePasswordAuthenticationToken.setAuthenticated(true);
		SecurityContextHolder.getContext().setAuthentication(new CammootionAuth(cammootionUser, authdataRecord));

		return cammootionUser;
	}
}
