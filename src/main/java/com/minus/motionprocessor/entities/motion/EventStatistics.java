package com.minus.motionprocessor.entities.motion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.DoubleSummaryStatistics;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

/**
 * Created by minus on 10/26/16.
 */
public class EventStatistics {
	private static final Logger logger = LoggerFactory.getLogger(EventStatistics.class);
	private final DoubleSummaryStatistics variation;
	private final Double stdDev;
	private final DoubleSummaryStatistics statistics;

	EventStatistics(Supplier<DoubleStream> deltas) {
		statistics = deltas.get().summaryStatistics();
		variation = deltas.get().map(x -> Math.pow(x - statistics.getAverage(), 2)).summaryStatistics();
		stdDev = Math.sqrt(variation.getSum() / (variation.getCount() - 1));
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("EventStatistics{");
		sb.append("average=").append(statistics.getAverage());
		sb.append(", stdDev=").append(stdDev);
		sb.append(", variation=").append(variation);
		sb.append('}');
		return sb.toString();
	}

	Double getStdDev() {
		return stdDev;
	}

	Double getAverage() {
		return statistics.getAverage();
	}
}
