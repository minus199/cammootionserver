package com.minus.motionprocessor.entities.motion;

import com.minus.motionprocessor.services.motion.analysis.Threshold;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.DoubleSummaryStatistics;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

/**
 * Created by minus on 10/29/16.
 */
class Statistics {
	private static final Logger logger = LoggerFactory.getLogger(EventStatistics.class);
	private final Threshold threshold;
	private final DoubleSummaryStatistics variation;
	private final Double stdDev;
	private final DoubleSummaryStatistics statistics;

	Statistics(Threshold threshold, Supplier<DoubleStream> deltas) {
		this.threshold = threshold;

		statistics = deltas.get().summaryStatistics();
		variation = deltas.get().map(x -> Math.pow(x - statistics.getAverage(), 2)).summaryStatistics();
		stdDev = Math.sqrt(variation.getSum() / (variation.getCount() - 1));

		logger.debug("Threshold: {} | Standard Deviation: {} | statistics: ",
		             threshold.value(), getStdDev(), this);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("EventStatistics{");
		sb.append("average=").append(statistics.getAverage());
		sb.append(", stdDev=").append(stdDev);
		sb.append(", variation=").append(variation);
		sb.append(", threshold=").append(threshold);
		sb.append('}');
		return sb.toString();
	}

	public Double getStdDev() {
		return stdDev;
	}

	public Threshold getThreshold() {
		return threshold;
	}

	public Double getAverage() {
		return statistics.getAverage();
	}
}
