package com.minus.motionprocessor.entities.motion;

import com.google.common.util.concurrent.AtomicDouble;

import java.io.File;
import java.nio.file.Path;

/**
 * Created by minus on 10/29/16.
 */
class EventFrame {

	private final File eventDirectory;
	private final String filePath;
	private final boolean isReady;
	private final AtomicDouble motionPercent;

	EventFrame(File eventDirectory, String filePath, boolean isReady, AtomicDouble motionPercent) {
		this.eventDirectory = eventDirectory;
		this.filePath = filePath;
		this.isReady = isReady;
		this.motionPercent = motionPercent;
	}

	Path getTargetFile() {
		String target = eventDirectory.getPath()
		                + File.separator +
		                getSrcFile().getName(getSrcFile().getNameCount() - 1);

		return new File(target).toPath();
	}

	Path getSrcFile() {
		return new File(filePath).toPath();
	}

	boolean isReady() {
		return isReady;
	}

	public AtomicDouble getMotionPercent() {
		return motionPercent;
	}
}
