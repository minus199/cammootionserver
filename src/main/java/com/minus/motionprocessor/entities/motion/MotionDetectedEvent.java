package com.minus.motionprocessor.entities.motion;

import com.minus.motionprocessor.entities.Callbacks;
import com.minus.motionprocessor.services.motion.analysisengine.Directories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

/**
 * Created by minus on 10/25/16.
 */
public class MotionDetectedEvent implements AutoCloseable {
	private static final Logger logger = LoggerFactory.getLogger(MotionDetectedEvent.class);

	private static final ReentrantLock currentTaskLock = new ReentrantLock();
	private static final long longThreshold = 10000;
	private static final Callbacks emtpyCallbacks = new Callbacks();
	private static TimerTask currentTask;
	private final ConcurrentLinkedDeque<Double> deltaPerFrame = new ConcurrentLinkedDeque<>();
	private final Timer timer = new Timer(true);
	private final ReentrantLock elapsedLock = new ReentrantLock();
	private final Supplier<File> eventOutputFileSupplier;
	private Callbacks.Callback ifNewEventCallback;
	private boolean isFinished = false;
	private long timeStarted = 0;
	private int finalizeDelay = 2000;
	private File eventOutputFile;
	private Callbacks callbacks;

	public MotionDetectedEvent(Callbacks.Callback ifNewEventCallback) {
		this();
		this.ifNewEventCallback = ifNewEventCallback;
	}

	public MotionDetectedEvent() {
		eventOutputFileSupplier = Directories::outputFile;
		eventOutputFile = eventOutputFileSupplier.get();
	}

	public void addFrame(double motionPercent) {
		ifNewEvent();
		deltaPerFrame.add(motionPercent);
		manageCurrentTask();
	}

	private boolean ifNewEvent() {
		if (timeStarted == 0) {
			elapsedLock.tryLock();
			timeStarted = System.currentTimeMillis();
			File eventOutputFile = this.eventOutputFile;
			this.eventOutputFile = eventOutputFileSupplier.get();
			logger.debug("Switched from file {} to file {}", eventOutputFile.getName(), this.eventOutputFile.getName());
			elapsedLock.unlock();

			ifNewEventCallback.call();

			return true;
		}

		return false;
	}

	public void finalizeEvent() {
		Callbacks.around(() -> Optional.of(_finalizeEvent()), getCallbacks());
	}

	private boolean _finalizeEvent() {
		EventStatistics statistics = getStatistics();

		logger.info("{} event [--> {} <--] finished | elapsed: {}, Average: {}, StdDev: {}",
		            isShort() ? "Short" : "Long",
		            eventOutputFile.getName(), (getElapsed() - finalizeDelay),
		            statistics.getAverage(), statistics.getStdDev());

		//prepare next event file
		timeStarted = 0;

		/*int initialSize = deltaPerFrame.size();
		deltaPerFrame.stream().parallel().forEach(eventFrame -> {
			try {
				Files.move(eventFrame.getSrcFile(), eventFrame.getTargetFile());
			} catch (IOException e) { e.printStackTrace(); }

			deltaPerFrame.remove(eventFrame);
		});

		logger.info("Copied {} frames to {}; Failed: {}", initialSize, eventOutputFile.getName(), deltaPerFrame.size());*/

		return isFinished = true;
	}

	private void manageCurrentTask() {
		currentTaskLock.tryLock();

		if (currentTask != null) currentTask.cancel();

		currentTask = new TimerTask() {
			@Override
			public void run() {
				finalizeEvent();
			}
		};

		timer.schedule(currentTask, finalizeDelay);
		currentTaskLock.unlock();
	}

	@Override
	public void close() throws Exception {
		timer.purge();
		timer.cancel();
		currentTask.run();
	}

	public File currentFilePath() {
		return eventOutputFile;
	}

	public final EventStatistics getStatistics() {
		return new EventStatistics(() -> deltaPerFrame.stream().mapToDouble(Double::doubleValue));
	}

	public final long getElapsed() {
		return System.currentTimeMillis() - timeStarted;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public boolean isShort() {
		return getElapsed() < longThreshold;
	}

	public Callbacks getCallbacks() {
		return callbacks == null ? emtpyCallbacks : callbacks;
	}

	public final void setFinalizeCallback(Callbacks callbacks) {
		this.callbacks = callbacks;
	}
}
