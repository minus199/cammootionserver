package com.minus.motionprocessor.entities;

import java.util.Optional;

/**
 * Created by minus on 10/29/16.
 */
public class Callbacks {
	private Callback enter;
	private Callback exit;
	private Callback failed;
	private Callback success;

	public Callbacks() {
		enter = null;
		exit = null;
		failed = null;
		success = null;
	}

	public Callbacks(Callback enter, Callback exit, Callback failed, Callback success) {
		this.enter = enter;
		this.exit = exit;
		this.failed = failed;
		this.success = success;
	}

	public static void around(Callback callback, Callbacks callbacks) {
		callbacks.getEnter().call();
		try {
			callback.call();
			callbacks.getSuccess();
		} catch (Exception ex) {
			callbacks.getFailed().call();
		} finally {
			callbacks.getExit().call();
		}
	}

	public interface Callback<T> {
		Optional<T> call();
	}

	public Callback getEnter() {
		return enter == null ? Optional::empty : enter;
	}

	public Callback getExit() {
		return exit == null ? Optional::empty : exit;
	}

	public Callback getFailed() {
		return failed == null ? Optional::empty : failed;
	}

	public Callback getSuccess() {
		return success == null ? Optional::empty : success;
	}

	public Callbacks setEnter(Callback enter) {
		this.enter = enter;
		return this;
	}

	public Callbacks setExit(Callback exit) {
		this.exit = exit;
		return this;
	}

	public Callbacks setFailed(Callback failed) {
		this.failed = failed;
		return this;
	}

	public Callbacks setSuccess(Callback success) {
		this.success = success;
		return this;
	}
}