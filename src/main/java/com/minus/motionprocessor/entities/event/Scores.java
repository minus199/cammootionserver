package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import org.jooq.types.UInteger;
import org.jooq.types.UShort;

/**
 * Created by minus on 10/2/16.
 */
public class Scores extends EventPart {
	public Scores(EventsRecord eventsRecord) {
		super(eventsRecord);
	}

	public UInteger getTotalScore() {
		return getEventsRecord().getTotscore();
	}

	public UShort getAvgScore() {
		return getEventsRecord().getAvgscore();
	}

	public UShort getMaxScore() {
		return getEventsRecord().getMaxscore();
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Scores{");
		sb.append("avgscore=").append(getAvgScore());
		sb.append(", maxscore=").append(getMaxScore());
		sb.append(", totscore=").append(getTotalScore());
		sb.append('}');
		return sb.toString();
	}

	public boolean isEmpty() {
		return getTotalScore() == null && getAvgScore() == null && getMaxScore() == null;
	}
}
