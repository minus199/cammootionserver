package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;

/**
 * Created by minus on 10/4/16.
 */
public class Computed extends EventPart {
	private final float framesRatio;
	private final float howLikely;

	public Computed(EventsRecord eventsRecord) {
		super(eventsRecord);

		float inBetweenFramesRatio;
		float inBetweenHowLikely;
		try {
			inBetweenFramesRatio = eventsRecord.getAlarmframes().floatValue() / eventsRecord.getFrames().floatValue();
			final float framesRatio2 = eventsRecord.getAlarmframes().floatValue()
					/ 100 /
					eventsRecord.getFrames().floatValue() * eventsRecord.getLength().floatValue();
			inBetweenHowLikely = framesRatio2 / inBetweenFramesRatio;
		} catch (NullPointerException e) {
			inBetweenFramesRatio = 0;
			inBetweenHowLikely = 0;
		}

		framesRatio = inBetweenFramesRatio;
		howLikely = inBetweenHowLikely;
	}

	public float getFramesRatio() {
		return framesRatio;
	}

	public float getHowLikely() {
		return howLikely;
	}
}
