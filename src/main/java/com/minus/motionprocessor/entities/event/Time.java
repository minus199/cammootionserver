package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;

import java.sql.Timestamp;

/**
 * Created by minus on 10/2/16.
 */
public class Time extends EventPart {


	public Time(EventsRecord eventsRecord) {
		super(eventsRecord);
	}

	public Timestamp getStartTime() {

		return getEventsRecord().getStarttime();
	}

	public Timestamp getEndTime() {
		return getEventsRecord().getEndtime();
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Time{");
		sb.append("endtime=").append(getEndTime());
		sb.append(", starttime=").append(getStartTime());
		sb.append('}');
		return sb.toString();
	}
}
