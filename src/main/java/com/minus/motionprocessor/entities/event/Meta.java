package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import org.jooq.types.UByte;

/**
 * Created by minus on 10/2/16.
 */
public class Meta extends EventPart {
	public Meta(EventsRecord eventsRecord) {
		super(eventsRecord);
	}

	public UByte getArchived() {
		return getEventsRecord().getArchived();
	}

	public UByte getVideoed() {
		return getEventsRecord().getVideoed();
	}

	public UByte getUploaded() {
		return getEventsRecord().getUploaded();
	}

	public UByte getEmailed() {
		return getEventsRecord().getEmailed();
	}

	public UByte getMessaged() {
		return getEventsRecord().getMessaged();
	}

	public UByte getExecuted() {
		return getEventsRecord().getExecuted();
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Meta{");
		sb.append("archived=").append(getArchived());
		sb.append(", emailed=").append(getEmailed());
		sb.append(", executed=").append(getExecuted());
		sb.append(", messaged=").append(getMessaged());
		sb.append(", uploaded=").append(getUploaded());
		sb.append(", videoed=").append(getVideoed());
		sb.append('}');
		return sb.toString();
	}
}
