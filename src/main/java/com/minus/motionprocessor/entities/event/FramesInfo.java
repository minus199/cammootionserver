package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import org.jooq.types.UInteger;
import org.jooq.types.UShort;

import java.math.BigDecimal;

/**
 * Created by minus on 10/2/16.
 */
public class FramesInfo extends EventPart {
	public FramesInfo(EventsRecord eventsRecord) {
		super(eventsRecord);
	}

	public UShort getWidth() {
		return getEventsRecord().getWidth();
	}

	public UShort getHeight() {
		return getEventsRecord().getHeight();
	}

	public BigDecimal getLength() {
		return getEventsRecord().getLength();
	}

	public UInteger getFrames() {
		return getEventsRecord().getFrames();
	}

	public UInteger getAlarmFrames() {
		return getEventsRecord().getAlarmframes();
	}

	//// TODO: 10/3/16 should be abstract method in eventpart
	public boolean isEmpty() {
		return getAlarmFrames() == null && getFrames() == null;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("FramesInfo{");
		sb.append("alarmframes=").append(getAlarmFrames());
		sb.append(", frames=").append(getFrames());
		sb.append(", height=").append(getHeight());
		sb.append(", length=").append(getLength());
		sb.append(", width=").append(getWidth());
		sb.append('}');
		return sb.toString();
	}
}
