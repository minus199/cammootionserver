package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.contracts.Mappable;
import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import com.minus.motionprocessor.utils.PackagingUtils;

import java.util.Map;

/**
 * Created by minus on 10/2/16.
 */
abstract class EventPart implements Mappable {
	private final EventsRecord eventsRecord;

	EventPart(EventsRecord eventsRecord) {
		this.eventsRecord = eventsRecord;
	}

	EventsRecord getEventsRecord() {
		return eventsRecord;
	}

	@Override
	public Map<String, ?> toMap() {
		return PackagingUtils.toMap(this);
	}
}
