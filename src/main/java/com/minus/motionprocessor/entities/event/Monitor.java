package com.minus.motionprocessor.entities.event;

import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import org.jooq.types.UInteger;

/**
 * Created by minus on 10/2/16.
 */
public class Monitor extends EventPart {

	public Monitor(EventsRecord eventsRecord) {
		super(eventsRecord);
	}

	public UInteger getMonitorid() {
		return getEventsRecord().getMonitorid();
	}

	public String getName() {
		return getEventsRecord().getName();
	}

	public String getNotes() {
		return getEventsRecord().getNotes();
	}

	public String getCause() {
		return getEventsRecord().getCause();
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Monitor{");
		sb.append("cause='").append(getCause()).append('\'');
		sb.append(", monitorid=").append(getMonitorid());
		sb.append(", name='").append(getName()).append('\'');
		sb.append(", notes='").append(getNotes()).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
