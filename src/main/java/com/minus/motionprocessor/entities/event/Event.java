package com.minus.motionprocessor.entities.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.minus.motionprocessor.contracts.Mappable;
import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import com.minus.motionprocessor.utils.PackagingUtils;
import org.jooq.types.UInteger;

import java.util.Map;

/**
 * Created by minus on 10/2/16.
 */
public class Event implements Mappable {
	private final Monitor monitor;
	private final Time eventTime;
	private final FramesInfo framesInfo;
	private final Scores scores;
	private final Meta meta;
	private final UInteger id;
	private final String eventView;
	private final Computed computed;

	public Event(EventsRecord eventsRecord) {
		this.monitor = new Monitor(eventsRecord);
		this.eventTime = new Time(eventsRecord);
		this.framesInfo = new FramesInfo(eventsRecord);
		this.scores = new Scores(eventsRecord);
		this.meta = new Meta(eventsRecord);
		this.id = eventsRecord.getId();
		this.eventView = "http://www.minus.dynu.com/zm/index.php?view=event&eid=" + id;

		// should always be last
		this.computed = new Computed(eventsRecord);
	}

	public Monitor getMonitor() {
		return monitor;
	}

	public Time getEventTime() {
		return eventTime;
	}

	public FramesInfo getFramesInfo() {
		return framesInfo;
	}

	public Scores getScores() {
		return scores;
	}

	public Meta getMeta() {
		return meta;
	}

	public UInteger getId() {
		return id;
	}

	private boolean isEmpty() {
		return getEventTime().getEndTime() == null || getFramesInfo().isEmpty() || getScores().isEmpty();
	}

	@Override
	public String toString() {
		return isEmpty() ? "" : "Event{" +
				"\n\t" + getEventTime().getStartTime().toString().split(" ")[1] +
				"To " + getEventTime().getEndTime().toString().split(" ")[1] +
				" [ F: " + getFramesInfo().getFrames() +
				" | FA: " + getFramesInfo().getAlarmFrames() +
				" | AS: " + getScores().getAvgScore() +
				" | MS: " + getScores().getMaxScore() +
				" | TS: " + getScores().getTotalScore() +
				"\n\tURL: " + getEventView() +
				"] \n\t}";
	}

	public String getEventView() {
		return eventView;
	}

	public EventPriority getPriority() {
		if (getComputed().getHowLikely() < 1) {
			if (getComputed().getHowLikely() < 0.1) {
				return EventPriority.Lowest_Priority;
			}

			if (getComputed().getHowLikely() < 0.15) {
				return EventPriority.Low_Priority;
			}

			return EventPriority.Normal_Priority;
		}

		return EventPriority.High_Priority;
	}

	public Computed getComputed() {
		return computed;
	}

	@Override
	public Map<String, ?> toMap() {
		return PackagingUtils.toMap(this);
	}

	public String toJson() {
		try {
			return new ObjectMapper().writeValueAsString(toMap());
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
}
