package com.minus.motionprocessor.entities.event;

/**
 * Created by minus on 10/2/16.
 */
public enum EventPriority {
	Lowest_Priority(-2),
	Low_Priority(-1),
	Normal_Priority(0),
	High_Priority(1);

	private final int intCode;

	EventPriority(int intCode) {
		this.intCode = intCode;
	}

	public String asCode() {
		return String.valueOf(intCode);
	}

	/**
	 *
	 * @param other to be compared with.
	 * @return 1 if current is larger. -1 if other. 0 otherwise.
	 */
	public int compare(EventPriority other){
		return intCode > other.intCode ? 1 : intCode < other.intCode ? -1 : 0;

	}
}
