package com.minus.motionprocessor.entities;

import java.sql.Timestamp;

/**
 * Created by minus on 10/4/16.
 */
public class PushEvent {
	private Integer alarmFrames;
	private Integer frames;

	private Integer avgScore;
	private Integer maxScore;

	private String startTime;
	private String endTime;

	private String url;

	public PushEvent() {
	}

	public Integer getAlarmFrames() {
		return alarmFrames;
	}

	public Integer getFrames() {
		return frames;
	}

	public Integer getAvgScore() {
		return avgScore;
	}

	public Integer getMaxScore() {
		return maxScore;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getUrl() {
		return url;
	}
}