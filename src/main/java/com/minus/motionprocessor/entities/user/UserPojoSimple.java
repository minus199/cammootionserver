package com.minus.motionprocessor.entities.user;

import com.minus.motionprocessor.db.mapping.tables.records.UsersRecord;

/**
 * Created by minus on 10/7/16.
 */
public class UserPojoSimple {
	private String displayName;
	private String email;
	private String familyName;
	private String givenName;
	private String id;
	private String idToken;
	private String photoUrl;
	private UsersRecord usersRecord;

	public UserPojoSimple() {
		/*usersRecord.getId();
		usersRecord.getUsername();
		usersRecord.getPassword();
		usersRecord.getLanguage();
		usersRecord.getEnabled();
		usersRecord.getStream();
		usersRecord.getEvents();
		usersRecord.getControl();
		usersRecord.getMonitors();
		usersRecord.getGroups();
		usersRecord.getDevices();
		usersRecord.getSystem();
		usersRecord.getMaxbandwidth();
		usersRecord.getMonitorids();*/
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
}
