package com.minus.motionprocessor.entities.user;

import com.minus.motionprocessor.db.mapping.tables.records.UsersRecord;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by minus on 10/6/16.
 */
public class CammootionUser extends User {

	private final UsersRecord userRecord;

	public CammootionUser(UsersRecord usersRecord, Collection<? extends GrantedAuthority> authorities) {
		super(usersRecord.getUsername(), usersRecord.getPassword(), authorities);
		this.userRecord = usersRecord;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return super.getAuthorities();
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
}
