package com.minus.motionprocessor.contracts;

import java.util.Map;

/**
 * Created by minus on 10/5/16.
 */
public interface Mappable {
	Map<String, ?> toMap();
}
