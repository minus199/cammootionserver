package com.minus.motionprocessor.tasks;

import com.minus.motionprocessor.services.frameswatcher.GifSequenceWriter;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Created by minus on 10/2/16.
 */
class GifWrapperTask implements Runnable {
	private static final int IMG_WIDTH = 800;
	private static final int IMG_HEIGHT = 600;

	private GifSequenceWriter gsw;
	private ConcurrentLinkedDeque<File> files;
	private File finalImage;

	GifWrapperTask(ConcurrentLinkedDeque<File> files) {
		this.files = files;
		System.out.println("running " + hashCode());
	}

	private static BufferedImage resizeImage(BufferedImage originalImage) {
		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, originalImage.getType());
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();

		return resizedImage;
	}

	private File createGifFile(String f, File readFromFile) {
		try {
			final File gifFile = new File(f);
			ImageIO.write(resizeImage(ImageIO.read(readFromFile)), "GIF", gifFile);
			return gifFile;
		} catch (Exception exc) {

		}

		throw new RuntimeException();
	}

	@Override
	public void run() {
		ImageOutputStream output;

		System.out.println("preparing");
		try {
			finalImage = createGifFile("/tmp/fuckedup.gif", files.getFirst());
			finalImage.createNewFile();
			BufferedImage firstFrame = ImageIO.read(files.getFirst());

			output = new FileImageOutputStream(finalImage);
			gsw = new GifSequenceWriter(output, firstFrame.getType(), 100, false);
		} catch (Exception e) {
			return;
		}

		System.out.println("starting processing");
		final int i = GifWrapperTask.this.hashCode();
		files.forEach(file -> {
			System.out.println(i + " | " + file.getAbsoluteFile());
			try {

				gsw.writeToSequence(resizeImage(ImageIO.read(file)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		try {
			System.out.println("Done.");
			output.close();
			gsw.close();
		} catch (IOException e) {
//			e.printStackTrace();
		}
	}
}