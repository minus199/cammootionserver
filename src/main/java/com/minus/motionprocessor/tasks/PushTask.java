package com.minus.motionprocessor.tasks;

import com.minus.motionprocessor.entities.Callbacks;
import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.entities.event.EventPriority;
import com.minus.motionprocessor.exceptions.EventMismatchException;
import com.minus.motionprocessor.services.EventsMatcher;
import com.minus.motionprocessor.services.pushNotifications.PusherOfNotifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/3/16.
 */
public class PushTask extends TimerTask {
	private static final Logger logger = LoggerFactory.getLogger(PushTask.class);
	private final ConcurrentLinkedDeque<File> files;
	private final Callbacks callbacks;

	public PushTask(ConcurrentLinkedDeque<File> files) {
		this(files, new Callbacks());
	}

	public PushTask(ConcurrentLinkedDeque<File> files, Callbacks callbacks) {
		this.files = new ConcurrentLinkedDeque<>(files.stream().sorted().collect(Collectors.toList()));

		this.callbacks = callbacks;
	}

	@Override
	public void run() {
		logger.debug("Enter PushTask");
		callbacks.getEnter().call();

		final List<File> captures = prepareForFilesMapping();
		logger.debug(__("PushTask captures: %d", captures.size()));

		try {
			Stream<Event> eventStream = EventsMatcher
					.match(captures.get(0), captures.get(captures.size() - 1));
//					.filter(e -> e.getPriority().compare(EventPriority.Low_Priority) > 0);

			PusherOfNotifications.push(PusherOfNotifications.PlatformAndroid, eventStream);
			callbacks.getSuccess().call();
		} catch (EventMismatchException e) {
			logger.error(__("Event mismatch: %s", captures.toString()));
			callbacks.getFailed().call();
		} finally {
			callbacks.getExit().call();
		}

//		new GifWrapperTask(new ConcurrentLinkedDeque<>(captures));

	}

	private List<File> prepareForFilesMapping() {
		final List<File> analyse = files.stream()
				.parallel()
				.filter(file -> file.getName().endsWith("-analyse.jpg"))
				.collect(Collectors.toList());

		return files.stream()
				.parallel()
				.filter(file -> file.getName().endsWith("-capture.jpg"))
				.map(file -> {
					// insert analysis images instead of capture
					final String s = file.getAbsolutePath().split("-")[0];
					return analyse.stream()
							.filter(f -> f.getAbsolutePath().startsWith(s))
							.findFirst().orElse(file);
				})
				.collect(Collectors.toList());
	}

	private String __(String message, Object... args) {
		return String.format(message, (Object[]) args);
	}




}
