package com.minus.motionprocessor.tasks;

import com.minus.motionprocessor.entities.motion.MotionDetectedEvent;
import com.minus.motionprocessor.services.motion.analysis.ComputedDiff;
import com.minus.motionprocessor.utils.ImgUtils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.imgcodecs.Imgcodecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

/**
 * Created by minus on 10/25/16.
 */
public class ImageWriterTask implements Runnable, AutoCloseable {
	private static final Logger logger = LoggerFactory.getLogger(ImageWriterTask.class);

	private final Mat mat;
	private final MotionDetectedEvent motionDetectedEvent;
	private MatOfInt params;
	private ComputedDiff computedDiff;

	public ImageWriterTask(Mat mat, MotionDetectedEvent e, MatOfInt params, ComputedDiff cd) {
		this.mat = mat;
		this.motionDetectedEvent = e;
		this.params = params;
		this.computedDiff = cd;
	}

	@Override
	public void run() {
		Path imgName = ImgUtils.imgPath(String.valueOf(computedDiff.getTotal()));
		boolean isWritten = Imgcodecs.imwrite(imgName.toString(), mat, params);
//		motionDetectedEvent.addFrame(imgName.toString(), isWritten, computedDiff);
	}

	@Override
	public void close() throws Exception {
		logger.debug("Closing task");
		motionDetectedEvent.finalizeEvent();
	}
}