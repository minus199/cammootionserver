package com.minus.motionprocessor.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by minus on 10/9/16.
 */
public class AuthRecordNotFoundException extends AuthenticationException{
	public AuthRecordNotFoundException() {
		super("Authentication failed. Go away.");
	}
}
