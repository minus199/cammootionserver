package com.minus.motionprocessor;

import com.minus.motionprocessor.db.mapping.Tables;
import com.minus.motionprocessor.db.mapping.tables.records.EventsRecord;
import com.minus.motionprocessor.entities.event.Event;
import com.minus.motionprocessor.exceptions.EventMismatchException;
import com.minus.motionprocessor.services.Ctx;
import com.minus.motionprocessor.services.EventsMatcher;
import com.minus.motionprocessor.services.frameswatcher.FrameWatcherTimer;
import com.minus.motionprocessor.services.frameswatcher.OngoingEventThread;
import com.minus.motionprocessor.services.motion.Camera;
import com.minus.motionprocessor.services.pushNotifications.PusherOfNotifications;
import org.jooq.Record;
import org.jooq.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by minus on 10/29/16.
 */
public class AppJunk {

	private static final Logger logger = LoggerFactory.getLogger(App.class);
	public static void main(String[] args) throws IOException, EventMismatchException {
//		PusherOfNotifications.push(PusherOfNotifications.PlatformAndroid, "Shit's still in progress.");
//		FramesWatcher.listen();
//		SpringApplication.run(App.class, args);

		// load the native OpenCV library
//		System.loadLibrary("/usr/local/share/OpenCV/java/libopencv_java310.so");
//		System.loadLibrary("opencv-310.jar");

//		launch(args);



		/*ScheduledFuture scheduledFuture =
				dispatcher.schedule(() -> System.out.println("Executed!"), 5, TimeUnit.SECONDS);*/

//		fixedRateAtBulk();
		/*consumer.shutdown();
		dispatcher.shutdown();
*/



/*		while (delta.get() < 5000L) {
			delta.set(System.currentTimeMillis() - l);
			logger.debug(String.valueOf(delta.get()));

			counter.incrementAndGet();
			consumer.submit(() -> {
				logger.debug(Thread.currentThread().getName());
				//worker takes a second to finish work
				try {
					Thread.sleep(10000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});

			// dispatch every 1 second
			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		logger.info(String.valueOf(counter.get()));
		consumer.shutdown();*/


	}
	private static void fixedRateAtBulk() {
		final long l = System.currentTimeMillis();
		final AtomicLong delta = new AtomicLong(System.currentTimeMillis() - l);
		final AtomicInteger counter = new AtomicInteger(0);

		ExecutorService consumer = Executors.newFixedThreadPool(10);
		ScheduledExecutorService dispatcher = Executors.newSingleThreadScheduledExecutor();

		dispatcher.scheduleAtFixedRate(() -> {
			delta.set(System.currentTimeMillis() - l);
			logger.debug("Delta is {}", String.valueOf(delta.get()));

			counter.incrementAndGet();
			consumer.submit(() -> {
				logger.debug("Current thread is {}", Thread.currentThread().getName());

				try {
					Thread.sleep(10000L); //worker takes X seconds to finish work
				} catch (InterruptedException e) { e.printStackTrace(); }
			});
		}, 0, 1, TimeUnit.SECONDS);

		logger.info(String.valueOf(counter.get()));
		try {
			consumer.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static void timerThread() {
//		final FrameWatcherTimer frameWatcherTimer = new FrameWatcherTimer(() -> foo == 0, t -> System.out.println("Bong"), 5000L);
		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(300L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
//				frameWatcherTimer.testReset();
//				foo = 1;
			}
		}).start();
	}

	private static void onGoingThread() {
		final OngoingEventThread ongoingEventThread = new OngoingEventThread();
		new Thread(ongoingEventThread).start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(3000L);
					} catch (InterruptedException e) {

					}
					System.out.println("slept woke up pausing");
					ongoingEventThread.pause();

					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {

					}
					System.out.println("slept woke up resuming");
					ongoingEventThread.resume();
				}
			}
		}).start();
	}

	private static void eveMatDbg() throws EventMismatchException {

		Stream<Event> match = EventsMatcher.match(
				new File("/mnt/47D93A6444EC43DB/events/1/16/10/13/00/20/25/00001-capture.jpg"),
				new File("/mnt/47D93A6444EC43DB/events/1/16/10/13/13/59/18/00165-capture.jpg")
		                                         );

		final Timestamp stimestamp = EventsMatcher.convertStringToTimestamp("2016-10-13 13:49:18", "yyyy-MM-dd HH:mm:ss");
		final Timestamp etimestamp = EventsMatcher.convertStringToTimestamp("2016-10-13 13:59:18", "yyyy-MM-dd HH:mm:ss");

		final List<String> collect = Ctx.use(ctx ->
				                                     ctx.select()
						                                     .from(Tables.EVENTS)
						                                     .where(Tables.EVENTS.STARTTIME.between(stimestamp).and(etimestamp))
						                                     .fetch().into(EventsRecord.class))
				.get()
				.stream().map(Event::new).map(Event::toJson).collect(Collectors.toList());

		System.exit(0);

		final Optional<Result<Record>> use = Ctx.use(ctx -> ctx.select()
				                                             .from(Tables.EVENTS)
				                                             .where(Tables.EVENTS.STARTTIME.between(stimestamp).and(etimestamp))
				                                             .fetch()
		                                            );
	}

	private static void handleUserAction(String[] args) {
		String action = args.length > 0 ? args[0] : "";

		switch (action) {
			case "dispatch":
				dispatch();
				break;

			case "edata":
				Ctx.use(ctx
						        -> ctx.select()
						.from(Tables.EVENTS)
						.fetch().into(EventsRecord.class)
						.stream().map(Event::new))
						.ifPresent(eventStream
								           -> eventStream
								.sorted((o1, o2)
										        -> o1.getComputed().getHowLikely() < o2.getComputed().getHowLikely()
										? - 1
										: o1.getComputed().getHowLikely() > o2.getComputed().getHowLikely()
										? 1 : 0)
								.forEach(event
										         -> System.out.printf("%-15s %-15s %15s %n", event.getPriority(), event.getComputed().getFramesRatio(), event.getComputed().getHowLikely())));

				break;
		}
	}

	private static void dispatch() {
		final Event event = Ctx.use(ctx
				                            -> ctx.select()
				                            .from(Tables.EVENTS)
				                            .limit(1)
				                            .fetchOneInto(EventsRecord.class)
		                           ).map(Event::new).orElseThrow(RuntimeException::new);

		PusherOfNotifications.push(PusherOfNotifications.PlatformAndroid, Collections.singletonList(event));
	}

	interface Bar {
		String t();
	}

	static class Foo {
		public Bar bar;
		public String a;
	}
}
