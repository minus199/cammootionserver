#!/bin/bash

echo "Preparing installation"
openCvFolder="OpenCV"
openCvArchive=${openCvFolder}.zip

mkdir -p ${openCvFolder} && cd ${openCvFolder}
wget -nc https://sourceforge.net/projects/opencvlibrary/files/latest/download -O ${openCvArchive}

openCvArchiveExtracted="$(dtrx -l ${openCvArchive} | head -n 1 | cut -d '/' -f 1)"
echo "$openCvArchiveExtracted will now be installed."
#http://manpages.ubuntu.com/manpages/wily/man1/dtrx.1.html -- extract here and rename to OpenCV, overwrite if exist
dtrx -n --one="rename" -o ${openCvArchive}
mv ${openCvFolder}/${openCvArchiveExtracted} ${openCvArchiveExtracted} && rm -rf ${openCvFolder} && cd ${openCvArchiveExtracted}

echo "Removing any pre-installed ffmpeg and x264"
sudo apt-get -qq remove ffmpeg x264 libx264-dev

echo "Installing Dependencies"
sudo apt-get update && sudo apt-get -qq install libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev \
libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine2-dev libgstreamer0.10-dev \
libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev \
libgtk2.0-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev \
libxvidcore-dev x264 v4l-utils ffmpeg cmake checkinstall dtrx

# incase qt needed -- qt5-default
#sudo apt-get install qt5-default checkinstall

mkdir -p build && cd build
currentPath=$(pwd)
./installCmake.sh
cd ${currentPath}

echo "Install ant if missing"
wget -nc http://apache.mivzakim.net//ant/binaries/apache-ant-1.9.7-bin.tar.gz
dtrx --one="here" -o -n apache-ant-1.9.7-bin.tar.gz
sudo mv apache-ant-1.9.7 /usr/local/share

echo "Making OpenCV"
export JAVA_HOME=$(readlink -f "$(which java)")
export ANT_HOME=/usr/local/share/apache-ant-1.9.7
export PATH=${ANT_HOME}/bin:${JAVA_HOME}/bin:${PATH}

#/usr/lib/jvm/jdk-8-oracle-arm32-vfp-hflt
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=OFF \
-D WITH_V4L=ON -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=OFF \
-D BUILD_EXAMPLES=OFF \
-D WITH_OPENGL=ON -DBUILD_SHARED_LIBS=OFF ..
#-D WITH_QT=ON \

make -j8
sudo checkinstall
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

mkdir -p /usr/local/share/OpenCV/java
sudo cp lib/libopencv_java310.so bin/opencv-310.jar /usr/local/share/OpenCV/java

echo "OpenCV ready to be used"