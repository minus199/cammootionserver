#!/bin/bash

baseURL="https://cmake.org/files"
echo "Validating cMake installation"

isCmake="$(which cmake)"
if [[ ${isCmake} == *"not found" ]] || [[ -z "${isCmake// }" ]]; then
	mkdir cmakeInstallation && cd cmakeInstallation
    latestVersion=$(wget -O- ${baseURL} | egrep 'v[0-9].[0-9]' |  awk '{print $5}' | awk -F "[(?<=\"\v)(?=\<\/)]" '{print $3}' | tail -n 1)
    latestVerPage="$baseURL/$latestVersion/?C=N;O=D"

    latestVersionNumber=$(echo ${latestVersion} | sed -e 's/v//g')
    fileName=$(wget -O- ${latestVerPage} | egrep "cmake-$latestVersionNumber" | head -n1 |  awk -F "[(?<=\"\v)(?=\<\/)]" '{print $21}')
    wget -nc ${baseURL}/${latestVersion}/${fileName} ${fileName}

	dtrx -v ${fileName}
	./bootstrap && make && sudo make install
fi

sudo dpkg --configure -a
echo "cMake validated"
