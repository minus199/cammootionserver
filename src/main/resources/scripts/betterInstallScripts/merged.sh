#!/usr/bin/env bash
# Get start time
dateformat="+%a %b %-eth %Y %I:%M:%S %p %Z"
starttime=$(date "$dateformat")
starttimesec=$(date +%s)

# Get current directory
curdir=$(cd `dirname $0` && pwd)

# Source config file
. "$curdir"/config-opencv.sh

# stdout and stderr for commands logged
logfile="$curdir/install-opencv.log"
rm -f $logfile

# Ubuntu version
ubuntuver=$DISTRIB_RELEASE

# Simple logger
log(){
	timestamp=$(date +"%m-%d-%Y %k:%M:%S")
	echo "$timestamp $1"
	echo "$timestamp $1" >> $logfile 2>&1
}

log "Installing OpenCV $opencvver on Ubuntu $ubuntuver $arch..."

# Remove temp dir
log "Removing temp dir $tmpdir"
rm -rf "$tmpdir"
mkdir -p "$tmpdir"

#Remove existing ffmpeg and other libs
log "Removing ffmpeg dependenices..."
apt-get -y autoremove ffmpeg x264 libav-tools libvpx-dev libx264-dev >> $logfile 2>&1

# Make sure root picks up JAVA_HOME for this process
export JAVA_HOME=$javahome
log "JAVA_HOME = $JAVA_HOME"

log "Installing OpenCV dependenices..."
 Install build tools
apt-get -y install build-essential pkg-config cmake yasm doxygen >> $logfile 2>&1

# Install media I/O libraries
apt-get -y install zlib1g-dev libjpeg-dev libwebp-dev libpng-dev libtiff-dev libjasper-dev libopenexr-dev libgdal-dev >> $logfile 2>&1

# Install video I/O libraries, support for Firewire video cameras and video streaming libraries
apt-get -y install libgtk2.0-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libopencore-amrnb-dev libopencore-amrwb-dev libv4l-dev libxine2-dev >> $logfile 2>&1

# Install the Python development environment and the Python Numerical library
apt-get -y install python-dev python-tk python-numpy python3-dev python3-tk python3-numpy >> $logfile 2>&1

# Install the parallel code processing and linear algebra library
apt-get -y install libtbb2 libtbb-dev libeigen3-dev >> $logfile 2>&1

# If not ARM then install the Qt library
if [ "$arch" != "armv7l" ] && [ "$arch" != "aarch64" ]; then
	apt-get -y install qt5-default libvtk6-dev >> $logfile 2>&1
fi

# Install libraries needed for contrib
apt-get -y install libprotobuf-dev protobuf-compiler

# Uninstall OpenCV if it exists
opencvhome="$HOME/opencv-$opencvver"
if [ -d "$opencvhome" ]; then
	log "Uninstalling OpenCV"
	cd "$opencvhome/build"
	make uninstall >> $logfile 2>&1
fi

# Download OpenCV source
cd "$tmpdir"
eval "$opencvcmd"
log "Removing $opencvhome"
rm -rf "$opencvhome"
log "Copying $tmpdir/opencv to $opencvhome"
cp -r "$tmpdir/opencv" "$opencvhome"


# Download OpenCV contrib source
opencvcontribhome="$HOME/opencv_contrib-$opencvver"
log "Removing $opencvcontribhome"
rm -rf "$opencvcontribhome"


# Patch jdhuff.c to remove "Invalid SOS parameters for sequential JPEG" warning
sed -i 's~WARNMS(cinfo, JWRN_NOT_SEQUENTIAL);~//WARNMS(cinfo, JWRN_NOT_SEQUENTIAL);\n      ; // NOP~g' "$opencvhome$jdhuff"

# Patch jdmarker.c to remove "Corrupt JPEG data: xx extraneous bytes before marker 0xd9" warning
#sed -i 's~WARNMS2(cinfo, JWRN_EXTRANEOUS_DATA~//WARNMS2(cinfo, JWRN_EXTRANEOUS_DATA~g' "$opencvhome$jdmarker"

# Patch gen_java.py to generate VideoWriter by removing from class_ignore_list
# This shouldn't be needed any more see https://github.com/Itseez/opencv/pull/5255
sed -i 's/\"VideoWriter\",/'\#\"VideoWriter\",'/g' "$opencvhome$genjava"

# Patch gen_java.py to generate constants by removing from const_ignore_list
sed -i 's/\"CV_CAP_PROP_FPS\",/'\#\"CV_CAP_PROP_FPS\",'/g' "$opencvhome$genjava"
sed -i 's/\"CV_CAP_PROP_FOURCC\",/'\#\"CV_CAP_PROP_FOURCC\",'/g' "$opencvhome$genjava"
sed -i 's/\"CV_CAP_PROP_FRAME_COUNT\",/'\#\"CV_CAP_PROP_FRAME_COUNT\",'/g' "$opencvhome$genjava"

# Patch gen_java.py to generate nativeObj as not final, so it can be modified by free() method
sed -i ':a;N;$!ba;s/protected final long nativeObj/protected long nativeObj/g' "$opencvhome$genjava"

# Patch gen_java.py to generate free() instead of finalize() methods
sed -i ':a;N;$!ba;s/@Override\n    protected void finalize() throws Throwable {\n        delete(nativeObj);\n    }/public void free() {\n        if (nativeObj != 0) {\n            delete(nativeObj);\n            nativeObj = 0;\n        }    \n    }/g' "$opencvhome$genjava"

# Patch gen_java.py to generate Mat.free() instead of Mat.release() methods
sed -i 's/mat.release()/mat.free()/g' "$opencvhome$genjava"

# Patch core+Mat.java remove final fron nativeObj, so new free() method can change
sed -i 's~public final long nativeObj~public long nativeObj~g' "$opencvhome$mat"

# Patch core+Mat.java to replace finalize() with free() method
sed -i ':a;N;$!ba;s/@Override\n    protected void finalize() throws Throwable {\n        n_delete(nativeObj);\n        super.finalize();\n    }/public void free() {\n        if (nativeObj != 0) {\n            release();\n            n_delete(nativeObj);\n            nativeObj = 0;\n        }    \n    }/g' "$opencvhome$mat"

# Patch utils+Converters.java to replace mi.release() with mi.free()
sed -i 's/mi.release()/mi.free()/g' "$opencvhome$converters"

# Compile OpenCV
log "Compile OpenCV...$opencvhome"
cd "$opencvhome"
mkdir build
cd build